<!DOCTYPE html>
<html lang="en">

@include('partials.head2')

<body>

    <!-- Back to Top Button -->
    <a id="top-button" href="#"><i class="fas fa-chevron-up"></i></a>
    <!-- End of Back to Top -->

    <!-- Preloader -->
    <div class="preloader-layout">
        <div class="cube-wrapper">
            <div class="cube-folding">
                <span class="leaf1"></span>
                <span class="leaf2"></span>
                <span class="leaf3"></span>
                <span class="leaf4"></span>
            </div>
            <span class="loading" data-name="loading">Cargando</span>
        </div>
    </div>

    <!-- 
    ======================================== 
        Header Section
    ========================================
    -->
    @include('partials.header2')
    <!-- 
    ======================================== 
        Content Section
    ========================================
    -->
    <div class="property-header-content pt-86 mb-50">
        <div class="container pt-60">
            <div class="row">
                <div class="col-md-10 pl-0">
                    <div class="heading-flex row">
                        <div class="col-md-8">
                            <h2>{{$property['nombre']}}.</h2>
                            <ul class="list-inline">
                                <li class="list-inline-item"><i class="fas fa-map-marker-alt primary-color"></i> {{$property['direccion']}}, {{$property->city->nombre}}</li>
                                <li class="list-inline-item heading-primary">Fecha: {{ date('d M Y - H:i:s', $property->created_at->timestamp) }}</li>
                            </ul>
                        </div>
                        <div class="col-md-4">
                            <div class="tag-group">
                                @if ($property['estado'] == 1)
                                    <a href="#" class="tag-blue" style="background-color: #3e4095">Alquiler</a>
                                @else
                                    <a href="#" class="tag-blue" style="background-color: #3e4095">Venta</a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 text-right pr-0">
                    <h2>{{$property->moneda()}} {{ number_format($property['precio'], 0 ) }}</h2>
                </div>
            </div>
            <div class="row mb-30">
                <div class="col-md-6 pl-0">
                    <ul class="list-inline">
                        {{-- <li class="list-inline-item">Types: <span class="heading-primary">Office</span></li> --}}
                        <li class="list-inline-item">Categoría: <span class="heading-primary">{{$property->category->nombre}}</span></li>
                    </ul>
                </div>
                <div class="col-md-6 justify-self-end text-right pr-0">
                    <div class="content-middle">
                        <p class="mb-0 d-inline-block"><i class="flaticon-building pr-1 primary-color"></i> Habitaciones: {{$property['habitaciones']}} / Camas: {{$property['camas']}} / Baños: {{$property['baños']}}</p>
                        <ul class="list-inline">
                            <!-- <li class="list-inline-item"><a href="#" class="favorite-feature"><i class="far fa-heart"></i></a></li>
                            <li class="list-inline-item"><a href="#" class="favorite-feature"><i class="fas fa-exchange-alt"></i></a></li> -->
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="property-gallery-layout mb-60">
        <div class="property-gallery-slider">
            <div class="gallery-item">
                <a data-fancybox="Property" href="/storage/propiedades/{{$property['img1']}}">
                    <img src="/storage/propiedades/{{$property['img1']}}" width="600" height="600" style="width:600px;height:600px;" alt="Clickhouse Inmobiliaria">
                </a>
            </div>
            @if ($property->img2)
                <div class="gallery-item">
                    <a data-fancybox="Property" href="/storage/propiedades/{{$property['img2']}}">
                        <img src="/storage/propiedades/{{$property['img2']}}" width="600" height="600" style="width:600px;height:600px;" alt="Clickhouse Inmobiliaria">
                    </a>
                </div>
            @endif
            @if ($property->img3)
                <div class="gallery-item">
                    <a data-fancybox="Property" href="/storage/propiedades/{{$property['img3']}}">
                        <img src="/storage/propiedades/{{$property['img3']}}" width="600" height="600" style="width:600px;height:600px;" alt="Clickhouse Inmobiliaria">
                    </a>
                </div>
            @endif
            @if ($property->img4)
                <div class="gallery-item">
                    <a data-fancybox="Property" href="/storage/propiedades/{{$property['img4']}}">
                        <img src="/storage/propiedades/{{$property['img4']}}" width="600" height="600" style="width:600px;height:600px;" alt="Clickhouse Inmobiliaria">
                    </a>
                </div>
            @endif
            @if ($property->img5)
                <div class="gallery-item">
                    <a data-fancybox="Property" href="/storage/propiedades/{{$property['img5']}}">
                        <img src="/storage/propiedades/{{$property['img5']}}" width="600" height="600" style="width:600px;height:600px;" alt="Clickhouse Inmobiliaria">
                    </a>
                </div>
            @endif
        </div>
    </div>

    <div class="property-main-content pb-100">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="single-item mb-30 intro-content">
                        <div class="heading">
                            <h4>Descripción de la Propiedad</h4>
                        </div>
                        <p>{{$property['descripcion']}}</p>
                        <!-- <a href="#" class="download primary-color"><i class="far fa-file-archive"></i> PDF Download</a> -->
                    </div>
                    <div class="single-item detail-feature mb-30">
                        <h4>Detalles</h4>
                        <div class="lists mb-30">
                            <ul class="detail-list">
                                <li><i class="fas fa-car-alt"></i> 
                                    Estacionamiento: 
                                    <span class="heading-primary">
                                        @if($property->cocina = 1)
                                        Si
                                        @else
                                        No
                                        @endif
                                    </span>
                                </li>
                                <li><i class="fas fa-bed"></i> Camas: <span class="heading-primary">{{$property['camas']}}</span></li>
                                <li><i class="fas fa-bath"></i> Baños: <span class="heading-primary">{{$property['baños']}}</span></li>
                                <li><i class="fas fa-map"></i> Tamaño: <span class="heading-primary">{{$property['m2']}}</span></li>
                                <li><i class="fas fa-door-open"></i> Habitaciones: <span class="heading-primary">{{$property['habitaciones']}}</span></li>
                            </ul>
                            <ul class="detail-list">
                                {{-- <li><i class="fas fa-home"></i> Built Year: <span class="heading-primary">09/05/2020</span></li> --}}
                                {{-- <li><i class="fas fa-arrows-alt"></i> Sq ft: <span class="heading-primary">200 sq ft</span></li> --}}
                                <li>
                                    <i class="fas fa-tv"></i> Living: 
                                    <span class="heading-primary">
                                        @if($property->living = 1)
                                        Si
                                        @else
                                        No
                                        @endif
                                    </span>
                                </li>
                                <li>
                                    <i class="fas fa-venus-double"></i> Cocina : 
                                    <span class="heading-primary">
                                        @if($property->cocina = 1)
                                        Si
                                        @else
                                        No
                                        @endif
                                    </span>
                                </li>
                                {{-- <li><i class="fas fa-torii-gate"></i> Oriantation: <span class="heading-primary">North</span></li> --}}
                            </ul>
                        </div>
                        {{-- <h4>Fecilities</h4>
                        <div class="lists">
                            <ul class="detail-list">
                                <li><i class="fas fa-check primary-color"></i> AC : Ceiling Fan(s), Central</li>
                                <li><i class="fas fa-check primary-color"></i> AP # : 7250-998-114</li>
                                <li><i class="fas fa-check primary-color"></i> Appliances : Gas, Dishwasher, <br>  Garbage Disposal</li>
                            </ul>
                            <ul class="detail-list">
                                <li><i class="fas fa-check primary-color"></i> Acres : 0.16</li>
                                <li><i class="fas fa-check primary-color"></i> Acres : 0.16</li>
                            </ul>
                        </div> --}}
                    </div>
                    {{-- <div class="single-item detail-video mb-30">
                        <h4>Propertis Video</h4>
                        <div class="video-item">
                            <img src="https://via.placeholder.com/710x375/ededed/999/" alt="Clickhouse Inmobiliaria" class="img-fluid">
                            <a data-fancybox="YouTube" href="https://www.youtube.com/watch?v=Maz7qkLzwYw" class="youtube-icon"><i class="fab fa-youtube"></i></a>
                        </div>
                    </div> --}}
                    {{-- <div class="single-item virtual-tour mb-30">
                        <h4>360° Virtual Tour</h4>
                        <img src="https://via.placeholder.com/710x375/ededed/999/" alt="Clickhouse Inmobiliaria" class="img-fluid">
                    </div> --}}
                    {{-- <div class="single-item mb-30">
                        <h4>Floor Plans</h4>
                        <div class="property-collapse">
                            <div class="collapse-item show">
                                <div class="head">
                                    <div class="left-head">
                                        <h5 class="mb-1">First Floor</h5>
                                        <p class="mb-0">Beds : 2  &nbsp;  Bath : 2  &nbsp;  Sqft : 750</p>
                                    </div>
                                    <div class="right-head">
                                        <a href="#" class="view-more collapse-click">View More</a>
                                    </div>
                                </div>
                                <div class="content">
                                    <img src="/img/property/map.png" alt="Clickhouse Inmobiliaria" class="img-fluid">
                                </div>
                            </div>

                            <div class="collapse-item">
                                <div class="head">
                                    <div class="left-head">
                                        <h5 class="mb-1">Second Floor</h5>
                                        <p class="mb-0">Beds : 2  &nbsp;  Bath : 2  &nbsp;  Sqft : 750</p>
                                    </div>
                                    <div class="right-head">
                                        <a href="#" class="view-more collapse-click">View More</a>
                                    </div>
                                </div>
                                <div class="content">
                                    <img src="/img/property/map.png" alt="Clickhouse Inmobiliaria" class="img-fluid">
                                </div>
                            </div>

                            <div class="collapse-item">
                                <div class="head">
                                    <div class="left-head">
                                        <h5 class="mb-1">Third Floor</h5>
                                        <p class="mb-0">Beds : 2  &nbsp;  Bath : 2  &nbsp;  Sqft : 750</p>
                                    </div>
                                    <div class="right-head">
                                        <a href="#" class="view-more collapse-click">View More</a>
                                    </div>
                                </div>
                                <div class="content">
                                    <img src="/img/property/map.png" alt="Clickhouse Inmobiliaria" class="img-fluid">
                                </div>
                            </div>
                        </div>
                    </div> --}}
                    <div class="single-item mb-30">
                        <h4 class="pb-2">Mapa</h4>
                        <div class="map-canvas" id="map-canvas" style="width: 100%; height: 350px;">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d209626.3583435141!2d-56.37652118443814!3d-34.820736164149814!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x959f80ffc63bf7d3%3A0x6b321b2e355bec99!2sMontevideo%2C%20Departamento%20de%20Montevideo%2C%20Uruguay!5e0!3m2!1ses!2sar!4v1595472322259!5m2!1ses!2sar" width="100%" height="350" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                        
                        </div>
                    </div>
                    {{-- <div class="single-item mb-30 thumbnail-items">
                        <h4>Real Estate</h4>
                        <div class="thumb-items mb-30">
                            <div class="thumb-item row mb-30">
                                <div class="col-8 d-flex item-details">
                                    <div class="img">
                                        <img src="https://via.placeholder.com/80/ededed/000/" alt="Clickhouse Inmobiliaria">
                                    </div>
                                    <div class="content align-self-center">
                                        <h6 class="mb-1"><a href="#">Kids Arning Center for</a></h6>
                                        <p class="mb-0">2500 Ponce De Leon Gables, FL 33134</p>
                                    </div>
                                </div>
                                <div class="col-4 align-self-center text-right">
                                    <p class="mb-1">1140 REVIEWS</p>
                                    <ul class="star-icons list-inline">
                                        <li class="list-inline-item"><i class="fas fa-star primary-color"></i></li>
                                        <li class="list-inline-item"><i class="fas fa-star primary-color"></i></li>
                                        <li class="list-inline-item"><i class="fas fa-star primary-color"></i></li>
                                        <li class="list-inline-item"><i class="fas fa-star primary-color"></i></li>
                                        <li class="list-inline-item"><i class="fas fa-star primary-color"></i></li>
                                    </ul>
                                </div>
                            </div>

                            <div class="thumb-item row mb-30">
                                <div class="col-8 d-flex item-details">
                                    <div class="img">
                                        <img src="https://via.placeholder.com/80/ededed/000/" alt="Clickhouse Inmobiliaria">
                                    </div>
                                    <div class="content align-self-center">
                                        <h6 class="mb-1"><a href="#">Center for Kids</a></h6>
                                        <p class="mb-0">2500 Ponce De Leon Gables, FL 33134</p>
                                    </div>
                                </div>
                                <div class="col-4 align-self-center text-right">
                                    <p class="mb-1">1140 REVIEWS</p>
                                    <ul class="star-icons list-inline">
                                        <li class="list-inline-item"><i class="fas fa-star primary-color"></i></li>
                                        <li class="list-inline-item"><i class="fas fa-star primary-color"></i></li>
                                        <li class="list-inline-item"><i class="fas fa-star primary-color"></i></li>
                                        <li class="list-inline-item"><i class="fas fa-star primary-color"></i></li>
                                        <li class="list-inline-item"><i class="fas fa-star primary-color"></i></li>
                                    </ul>
                                </div>
                            </div>

                            <div class="thumb-item row">
                                <div class="col-8 d-flex item-details">
                                    <div class="img">
                                        <img src="https://via.placeholder.com/80/ededed/000/" alt="Clickhouse Inmobiliaria">
                                    </div>
                                    <div class="content align-self-center">
                                        <h6 class="mb-1"><a href="#">Kids Arning Center for</a></h6>
                                        <p class="mb-0">2500 Ponce De Leon Gables, FL 33134</p>
                                    </div>
                                </div>
                                <div class="col-4 align-self-center text-right">
                                    <p class="mb-1">1140 REVIEWS</p>
                                    <ul class="star-icons list-inline">
                                        <li class="list-inline-item"><i class="fas fa-star primary-color"></i></li>
                                        <li class="list-inline-item"><i class="fas fa-star primary-color"></i></li>
                                        <li class="list-inline-item"><i class="fas fa-star primary-color"></i></li>
                                        <li class="list-inline-item"><i class="fas fa-star primary-color"></i></li>
                                        <li class="list-inline-item"><i class="fas fa-star primary-color"></i></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <h4>Restaurants</h4>
                        <div class="thumb-items">
                            <div class="thumb-item row mb-30">
                                <div class="col-8 d-flex item-details">
                                    <div class="img">
                                        <img src="https://via.placeholder.com/80/ededed/000/" alt="Clickhouse Inmobiliaria">
                                    </div>
                                    <div class="content align-self-center">
                                        <h6 class="mb-1"><a href="#">Fog Harbor Fish House</a></h6>
                                        <p class="mb-0">2500 Ponce De Leon Gables, FL 33134</p>
                                    </div>
                                </div>
                                <div class="col-4 align-self-center text-right">
                                    <p class="mb-1">1140 REVIEWS</p>
                                    <ul class="star-icons list-inline">
                                        <li class="list-inline-item"><i class="fas fa-star primary-color"></i></li>
                                        <li class="list-inline-item"><i class="fas fa-star primary-color"></i></li>
                                        <li class="list-inline-item"><i class="fas fa-star primary-color"></i></li>
                                        <li class="list-inline-item"><i class="fas fa-star primary-color"></i></li>
                                        <li class="list-inline-item"><i class="fas fa-star primary-color"></i></li>
                                    </ul>
                                </div>
                            </div>

                            <div class="thumb-item row mb-30">
                                <div class="col-8 d-flex item-details">
                                    <div class="img">
                                        <img src="https://via.placeholder.com/80/ededed/000/" alt="Clickhouse Inmobiliaria">
                                    </div>
                                    <div class="content align-self-center">
                                        <h6 class="mb-1"><a href="#">Authentic Turkish Restaurant</a></h6>
                                        <p class="mb-0">2500 Ponce De Leon Gables, FL 33134</p>
                                    </div>
                                </div>
                                <div class="col-4 align-self-center text-right">
                                    <p class="mb-1">1140 REVIEWS</p>
                                    <ul class="star-icons list-inline">
                                        <li class="list-inline-item"><i class="fas fa-star primary-color"></i></li>
                                        <li class="list-inline-item"><i class="fas fa-star primary-color"></i></li>
                                        <li class="list-inline-item"><i class="fas fa-star primary-color"></i></li>
                                        <li class="list-inline-item"><i class="fas fa-star primary-color"></i></li>
                                        <li class="list-inline-item"><i class="fas fa-star primary-color"></i></li>
                                    </ul>
                                </div>
                            </div>

                            <div class="thumb-item row">
                                <div class="col-8 d-flex item-details">
                                    <div class="img">
                                        <img src="https://via.placeholder.com/80/ededed/000/" alt="Clickhouse Inmobiliaria">
                                    </div>
                                    <div class="content align-self-center">
                                        <h6 class="mb-1"><a href="#">The Front Porch</a></h6>
                                        <p class="mb-0">2500 Ponce De Leon Gables, FL 33134</p>
                                    </div>
                                </div>
                                <div class="col-4 align-self-center text-right">
                                    <p class="mb-1">1140 REVIEWS</p>
                                    <ul class="star-icons list-inline">
                                        <li class="list-inline-item"><i class="fas fa-star primary-color"></i></li>
                                        <li class="list-inline-item"><i class="fas fa-star primary-color"></i></li>
                                        <li class="list-inline-item"><i class="fas fa-star primary-color"></i></li>
                                        <li class="list-inline-item"><i class="fas fa-star primary-color"></i></li>
                                        <li class="list-inline-item"><i class="fas fa-star primary-color"></i></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div> --}}
                    {{-- <div class="single-item property-form">
                        <h4>Leave A Reply</h4>
                        <p>* Your Email Address will not be published</p>
                        <form>
                            <ul class="list-inline">
                                <li class="list-inline-item pr-3 heading-primary">Rating:</li>
                                <li class="list-inline-item"><i class="fas fa-star primary-color"></i></li>
                                <li class="list-inline-item"><i class="fas fa-star primary-color"></i></li>
                                <li class="list-inline-item"><i class="fas fa-star primary-color"></i></li>
                                <li class="list-inline-item"><i class="fas fa-star primary-color"></i></li>
                                <li class="list-inline-item"><i class="fas fa-star primary-color"></i></li>
                            </ul>
                            <div class="input-full">
                                <textarea name="message" placeholder="Your Message"></textarea>
                            </div>
                            <div class="input-half">
                                <input type="text" name="name" placeholder="Your Name">
                            </div>
                            <div class="input-half">
                                <input type="email" name="email" placeholder="Your Email">
                            </div>
                            <div class="input-submit">
                                <button type="submit" class="button button-primary">Send Message</button>
                            </div>
                        </form>
                    </div> --}}
                </div>
                <div class="col-md-4">
                    <div class="property-sidebar">
                        <div class="sidebar-item item-one mb-30">
                            <div class="sidebar-author">
                                <img src="/storage/propiedades/{{$property['img1']}}" alt="Clickhouse Inmobiliaria" width="370" height="270" style="width:370px;height:270px;" class="img-fluid">
                                <div class="hover">
                                    {{-- <div class="author d-flex">
                                        <div class="img">
                                            <img src="/img/property/thumb-6.png" alt="Clickhouse Inmobiliaria">
                                        </div>
                                        <h6 class="mb-0 align-self-center color-white">Dilliama Nelissa</h6>
                                    </div> --}}
                                </div>
                            </div>
                            <div class="contact">
                                <form>
                                    <input type="text" name="name" placeholder="Nombre*">
                                    <input type="email" name="email" placeholder="Email*">
                                    <input type="propiedad" name="propiedad" hidden value="{{$property['nombre']}}">
                                    <textarea name="message" placeholder="Mensaje*"></textarea>
                                    <button type="submit" class="d-block button button-primary">Enviar</button>
                                </form>
                            </div>
                        </div>
                        {{-- <div class="sidebar-item item-two">
                            <h4>Recently Viewed</h4>
                            <div class="recent-item d-flex mb-30">
                                <div class="img">
                                    <img src="https://via.placeholder.com/75/ededed/000/" alt="Clickhouse Inmobiliaria">
                                </div>
                                <div class="content align-self-center">
                                    <h6 class="mb-1"><a href="#">Home in Merrick Way</a></h6>
                                    <p class="mb-0">$54,000</p>
                                </div>
                            </div>

                            <div class="recent-item d-flex mb-30">
                                <div class="img">
                                    <img src="https://via.placeholder.com/75/ededed/000/" alt="Clickhouse Inmobiliaria">
                                </div>
                                <div class="content align-self-center">
                                    <h6 class="mb-1"><a href="#">Villa on Grand Avenue</a></h6>
                                    <p class="mb-0">$54,000</p>
                                </div>
                            </div>

                            <div class="recent-item d-flex">
                                <div class="img">
                                    <img src="https://via.placeholder.com/75/ededed/000/" alt="Clickhouse Inmobiliaria">
                                </div>
                                <div class="content align-self-center">
                                    <h6 class="mb-1"><a href="#">Blue Pearl Therapeutic</a></h6>
                                    <p class="mb-0">$54,000</p>
                                </div>
                            </div>
                        </div> --}}
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- 
    ======================================== 
        Footer Section
    ========================================
    -->
    @include('partials.footer2')

    <!-- Scripts -->
    <script src="/lib/jquery-3.4.1.min.js"></script>
    <script src="/lib/jquery-ui.min.js"></script>
    <script src="/lib/bootstrap/js/popper.min.js"></script>
    <script src="/lib/bootstrap/js/bootstrap.min.js"></script>
    <script src="/lib/counterup/waypoints.min.js"></script>
    <script src="/lib/counterup/jquery.counterup.min.js"></script>
    <script src="/lib/fancybox/jquery.fancybox.min.js"></script>
    <script src="/lib/Menuzord/js/menuzord.js"></script>
    <script src="/lib/isotope/isotope.min.js"></script>
    <script src="/lib/aos/aos.js"></script>
    <script src="/lib/retina/retina.min.js"></script>
    <script src="/lib/slick/slick.min.js"></script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAFxhUVMwiYKz3ii7f3PL1_YYih3fOtg-M"></script>
    <script src="/lib/maps/footer-map.js"></script>
    <script src="/lib/maps/short-map.js"></script>

    <!-- Custom Scripts -->
    <script src="/js/behome.js"></script>
    <script src="/js/slider.js"></script>

    <!-- Off Canvas  -->
    @include('partials.panel-contacto')
    <a href="https://api.whatsapp.com/send?text=&phone=%2B598098569931" class="whatsapp" target="_blank"> <i class="fa fa-whatsapp whatsapp-icon"></i></a>
    
</body>
</html>