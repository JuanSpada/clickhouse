   
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="img/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="img/favicon-16x16.png">

    <!-- Fonts -->
    <link href="//fonts.googleapis.com/css2?family=Raleway:wght@400;500;600;700;800&display=swap" rel="stylesheet">

    <!-- All Styles -->
    <link rel="stylesheet" href="lib/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="lib/fontawesome/css/all.min.css">
    <link rel="stylesheet" href="lib/iconfont/flaticon.css">
    <link rel="stylesheet" href="lib/Menuzord/css/menuzord.css">
    <link rel="stylesheet" href="lib/slick/slick.css">
    <link rel="stylesheet" href="lib/slick/slick-theme.css">
    <link rel="stylesheet" href="lib/fancybox/jquery.fancybox.css">
    <link rel="stylesheet" href="lib/animate/animate.css">
    <link rel="stylesheet" href="lib/aos/aos.css">

    <!-- Theme Styels -->
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/responsive.css">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Color Style -->
    <!-- <link rel="stylesheet" href="css/light-blue.css">
    <link rel="stylesheet" href="css/orange-color.css">
    <link rel="stylesheet" href="css/blue-color.css">
    <link rel="stylesheet" href="css/green-color.css">
    <link rel="stylesheet" href="css/primary-color.css"> -->

    <title>Clickhouse Inmobiliaria</title>

    <!-- SEO Meta Tags -->
    <meta name="description" content="Creemos que las buenas prácticas, el marketing dedicado, la atención personalizada y profesional, hacen el marco perfecto para ser la mejor opción para nuestros clientes.
    Entendemos que compensamos perfectamente el hecho de ser “jóvenes” en un mercado donde existen muchas empresas de larga data con la frescura y el agiornamiento a los tiempos modernos.">
    <meta name="keywords" content="clickhouse inmobiliaria">
</head>