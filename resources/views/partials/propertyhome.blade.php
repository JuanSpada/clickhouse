
<section id="property" class="property-layout-one pa-100">

    @isset($properties_principales[0])
        <div class="property-list-one pb-100">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="section-head mb-40">
                            <h2 data-title="01">{{$properties_principales[0]->direccion}}, {{$properties_principales[0]->city->nombre}}</h2>
                            <p><b>{{$properties_principales[0]->nombre}}</b></p>
                            <p class="mb-0">{{$properties_principales[0]->descripcion}}</p>
                        </div>
                        <div class="row mb-60">
                            <div class="col-md-6">
                                <div class="property-item d-flex">
                                    <div class="icon">
                                        <i class="flaticon-shrink"></i>
                                    </div>
                                    <div class="content">
                                        <h6>Tamaño en m2:</h6>
                                        <p class="mb-0">{{$properties_principales[0]->m2}}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="property-item d-flex">
                                    <div class="icon">
                                        <i class="flaticon-bed"></i>
                                    </div>
                                    <div class="content">
                                        <h6>Estructura:</h6>
                                        <p class="mb-0">{{$properties_principales[0]->habitaciones}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-40">
                            <div class="col-md-6">
                                <div class="property-item d-flex">
                                    <div class="icon">
                                        <i class="flaticon-bike"></i>
                                    </div>
                                    <div class="content">
                                        <h6>Cochera:</h6>
                                        <p class="mb-0">{{$properties_principales[0]->estacionamientos}}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="property-item d-flex">
                                    <div class="icon">
                                        <i class="flaticon-shower"></i>
                                    </div>
                                    <div class="content">
                                        <h6>Baños:</h6>
                                        <p class="mb-0">{{$properties_principales[0]->baños}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a href="/propiedades/{{$properties_principales[0]->id}}" class="button button-primary button-white">Ver Más</a>
                    </div>
                </div>
                <div class="property-sliding-section">
                    <div class="property-carousel property-slide-one">
                        <div class="property-slide">
                            <img src="/storage/propiedades/{{$properties_principales[0]->img1}}" alt="Clickhouse Inmobiliaria" class="img-fluid">
                        </div>
                        @if ($properties_principales[0]->img2)
                            <div class="property-slide">
                                <img src="/storage/propiedades/{{$properties_principales[0]->img2}}" alt="Clickhouse Inmobiliaria" class="img-fluid">
                            </div>
                        @endif
                        
                        @if ($properties_principales[0]->img3)
                            <div class="property-slide">
                                <img src="/storage/propiedades/{{$properties_principales[0]->img3}}" alt="Clickhouse Inmobiliaria" class="img-fluid">
                            </div>
                        @endif
                    </div>
                    <div class="property-navigation property-nav-one">
                        @if ($properties_principales[0]->img2)
                            <div class="property-nav-item">
                                <img src="/storage/propiedades/{{$properties_principales[0]->img1}}" alt="Clickhouse Inmobiliaria">
                            </div>
                            <div class="property-nav-item">
                                <img src="/storage/propiedades/{{$properties_principales[0]->img2}}" alt="Clickhouse Inmobiliaria">
                            </div>
                        @endif
                        @if ($properties_principales[0]->img3)
                            <div class="property-nav-item">
                                <img src="/storage/propiedades/{{$properties_principales[0]->img3}}" alt="Clickhouse Inmobiliaria">
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    @endisset

    @isset($properties_principales[1])
        <div class="property-list-two pa-100">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 ml-auto">
                        <div class="section-head mb-40">
                            <h2 data-title="02">{{$properties_principales[1]->direccion}}, {{$properties_principales[1]->city->nombre}} </h2>
                            <p><b>{{$properties_principales[1]->nombre}}</b></p>
                            <p class="mb-0">{{$properties_principales[1]->descripcion}}</p>
                        </div>
                        <div class="row mb-60">
                            <div class="col-md-6">
                                <div class="property-item d-flex">
                                    <div class="icon">
                                        <i class="flaticon-shrink"></i>
                                    </div>
                                    <div class="content">
                                        <h6>Tamaño en m2:</h6>
                                        <p class="mb-0">{{$properties_principales[1]->m2}}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="property-item d-flex">
                                    <div class="icon">
                                        <i class="flaticon-bed"></i>
                                    </div>
                                    <div class="content">
                                        <h6>Estructura:</h6>
                                        <p class="mb-0">{{$properties_principales[1]->habitaciones}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-40">
                            <div class="col-md-6">
                                <div class="property-item d-flex">
                                    <div class="icon">
                                        <i class="flaticon-bike"></i>
                                    </div>
                                    <div class="content">
                                        <h6>Cochera:</h6>
                                        <p class="mb-0">{{$properties_principales[1]->estacionamientos}}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="property-item d-flex">
                                    <div class="icon">
                                        <i class="flaticon-shower"></i>
                                    </div>
                                    <div class="content">
                                        <h6>Baños:</h6>
                                        <p class="mb-0">{{$properties_principales[1]->baños}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a href="/propiedades/{{$properties_principales[1]->id}}" class="button button-primary button-white">Ver Más</a>
                    </div>
                </div>

                <div class="property-sliding-section">
                    <div class="property-carousel property-slide-two">
                        <div class="property-slide">
                            <img src="/storage/propiedades/{{$properties_principales[1]->img1}}" alt="Clickhouse Inmobiliaria" class="img-fluid">
                        </div>
                        @if ($properties_principales[1]->img2)
                            <div class="property-slide">
                                <img src="/storage/propiedades/{{$properties_principales[1]->img2}}" alt="Clickhouse Inmobiliaria" class="img-fluid">
                            </div>
                        @endif
                        
                        @if ($properties_principales[1]->img3)
                            <div class="property-slide">
                                <img src="/storage/propiedades/{{$properties_principales[1]->img3}}" alt="Clickhouse Inmobiliaria" class="img-fluid">
                            </div>
                        @endif
                    </div>
                    <div class="property-navigation property-nav-two">
                        @if ($properties_principales[1]->img2)
                            <div class="property-nav-item">
                                <img src="/storage/propiedades/{{$properties_principales[1]->img1}}" alt="Clickhouse Inmobiliaria">
                            </div>
                            <div class="property-nav-item">
                                <img src="/storage/propiedades/{{$properties_principales[1]->img2}}" alt="Clickhouse Inmobiliaria">
                            </div>
                        @endif
                        @if ($properties_principales[1]->img3)
                            <div class="property-nav-item">
                                <img src="/storage/propiedades/{{$properties_principales[1]->img3}}" alt="Clickhouse Inmobiliaria">
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        
    @endisset

    @isset($properties_principales[2])
        <div class="property-list-three pt-100">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="section-head mb-40">
                            <h2 data-title="03">{{$properties_principales[2]->direccion}}, {{$properties_principales[2]->city->nombre}}</h2>
                            <p><b>{{$properties_principales[2]->nombre}}</b></p>
                            <p class="mb-0">{{$properties_principales[2]->descripcion}}</p>
                        </div>
                        <div class="row mb-60">
                            <div class="col-md-6">
                                <div class="property-item d-flex">
                                    <div class="icon">
                                        <i class="flaticon-shrink"></i>
                                    </div>
                                    <div class="content">
                                        <h6>Tamaño en m2:</h6>
                                        <p class="mb-0">{{$properties_principales[2]->m2}}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="property-item d-flex">
                                    <div class="icon">
                                        <i class="flaticon-bed"></i>
                                    </div>
                                    <div class="content">
                                        <h6>Estructura:</h6>
                                        <p class="mb-0">{{$properties_principales[2]->habitaciones}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-40">
                            <div class="col-md-6">
                                <div class="property-item d-flex">
                                    <div class="icon">
                                        <i class="flaticon-bike"></i>
                                    </div>
                                    <div class="content">
                                        <h6>Cochera:</h6>
                                        <p class="mb-0">{{$properties_principales[2]->estacionamientos}}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="property-item d-flex">
                                    <div class="icon">
                                        <i class="flaticon-shower"></i>
                                    </div>
                                    <div class="content">
                                        <h6>Baños:</h6>
                                        <p class="mb-0">{{$properties_principales[2]->baños}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a href="/propiedades/{{$properties_principales[2]->id}}" class="button button-primary button-white">Ver Más</a>
                    </div>
                </div>
                <div class="property-sliding-section">
                    <div class="property-carousel property-slide-three">
                        <div class="property-slide">
                            <img src="/storage/propiedades/{{$properties_principales[2]->img1}}" alt="Clickhouse Inmobiliaria" class="img-fluid">
                        </div>
                        @if ($properties_principales[2]->img2)
                            <div class="property-slide">
                                <img src="/storage/propiedades/{{$properties_principales[2]->img2}}" alt="Clickhouse Inmobiliaria" class="img-fluid">
                            </div>
                        @endif
                        
                        @if ($properties_principales[2]->img3)
                            <div class="property-slide">
                                <img src="/storage/propiedades/{{$properties_principales[2]->img3}}" alt="Clickhouse Inmobiliaria" class="img-fluid">
                            </div>
                        @endif
                    </div>
                    <div class="property-navigation property-nav-three">
                        @if ($properties_principales[2]->img2)
                            <div class="property-nav-item">
                                <img src="/storage/propiedades/{{$properties_principales[2]->img1}}" alt="Clickhouse Inmobiliaria">
                            </div>
                            <div class="property-nav-item">
                                <img src="/storage/propiedades/{{$properties_principales[2]->img2}}" alt="Clickhouse Inmobiliaria">
                            </div>
                        @endif
                        @if ($properties_principales[2]->img3)
                            <div class="property-nav-item">
                                <img src="/storage/propiedades/{{$properties_principales[2]->img3}}" alt="Clickhouse Inmobiliaria">
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        
    @endisset

</section>