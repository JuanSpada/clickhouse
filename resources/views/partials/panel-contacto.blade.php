<div id="mySidenav" class="sidenav">
    <div class="sidenav-menu" id="innar-side">
        <a href="#" class="closebtn bg-orange"><i class="flaticon-cancel"></i></a>
        <div class="side-form">

            <img src="/img/logo-recto.png" style="width:206px;height:43px;" alt="Clickhouse Inmobiliaria" data-rjs="3">
            <h4 class="pt-4 text-left">Bienvenido/a</h4>
            <p class="my-0 pb-5 pt-0 text-left">Tenemos la experiencia y el conocimiento profundo del mercado.</p>
            <a href="/contacto" class="button button-primary button-rounded">Contactanos</a>
        </div>
        <ul class="sidenav-list">
            <li class="mt-5">
                <h5 class="pb-2">Información de Contacto</h5>
                <div class="info-list d-flex">
                    <div class="icon">
                        <i class="fas fa-map-marked-alt"></i>
                    </div>
                    <div class="content">
                        <p>Dirección</p>
                    </div>
                </div>
                <div class="info-list d-flex">
                    <div class="icon">
                        <i class="fas fa-envelope"></i>
                    </div>
                    <div class="content">
                        <p>info@clickhouseinmobiliaria.com</p>
                    </div>
                </div>
                <div class="info-list d-flex">
                    <div class="icon">
                        <i class="fas fa-phone-alt"></i>
                    </div>
                    <div class="content">
                        <p>098 569 931</p>
                    </div>
                </div>
                <div class="info-list d-flex">
                    <div class="icon">
                        <i class="fas fa-calendar-check"></i>
                    </div>
                    <div class="content">
                        <p>Lunes-Viernes: 9.00AM to 6.00PM</p>
                    </div>
                </div>
                <ul class="social">
                    <li><a href="https://www.facebook.com/click.house.58" class="color-facebook"><i class="fab fa-facebook-f"></i></a></li>
                    <li><a href="https://www.instagram.com/house.click/" class=""><i class="fab fa-instagram"></i></a></li>
                    <li><a href="https://www.instagram.com/house.click/" class=""><i class="fab fa-twitter"></i></a></li>
                </ul>
            </li>
        </ul>

    </div>
</div>