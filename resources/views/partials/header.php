<header id="header" class="header-layout-one unveiled-navigation">

    <!-- Header Top -->
    <div class="top-bar" id="topbar">
        <div class="container">
            <div class="row">
                <div class="col-md-9 align-self-center">
                    <div class="bar-left">
                        <ul class="list-inline">
                            <li class="list-inline-item color-white mr-4"><i class="fas fa-phone-alt mr-1"></i>098 569 931</li>
                            <li class="list-inline-item color-white"><i class="far fa-envelope-open mr-1"></i> info@clickhouseinmobiliaria.com</li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 text-right">
                    <ul class="list-inline top-social">
                        <li class="list-inline-item">
                            <a href="https://www.facebook.com/click.house.58"><i class="fab fa-facebook-f"></i></a>
                        </li>
                        <li class="list-inline-item">
                            <a href="https://www.instagram.com/house.click/"><i class="fab fa-instagram"></i></a>
                        </li>
                        <li class="list-inline-item">
                            <a href="https://www.instagram.com/house.click/"><i class="fab fa-twitter"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <!-- Header Middle -->
    <div class="container text-center header-middle">
        <a href="#"><img src="/img/logo-recto-blanco.png" alt="Clickhouse Inmobiliaria" style="width:206px; height: 43px;" data-rjs="3"></a>
    </div>

    <!-- Header Bottom -->
    <div id="header-bottom-two">
        <div class="container">
            <div class="row">
                <div class="col-md-8 mx-auto">
                    <div class="menuzord main-navigation" id="menuzord">
                        <a href="/" class="menuzord-brand hide-normal"><img src="/img/logo-recto-blanco.png" style="width:206px; height: 43px;" alt="Clickhouse Inmobiliaria"  data-rjs="3"></a>
                        <ul class="menuzord-menu menuzord-right">
                            <li class="active"><a href="#">Inicio</a></li>
                            <li><a href="/propiedades">Propiedades</a>
                                <ul class="dropdown">
                                    <li><a href="/propiedades?estado=1">Alquiler</a></li>
                                    <li><a href="/propiedades?estado=2">Venta</a></li>
                                </ul>
                            </li>
                            <li><a href="#about">Nosotros</a></li>
                            <li><a href="/contacto">Contacto</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End of Header Bottom -->
</header>