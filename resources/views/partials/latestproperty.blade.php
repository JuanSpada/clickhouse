<section id="latest-property" class="latest-property-layout feature-layout-one pa-100">
    <div class="container">
        <div class="row mb-60">
            <div class="col-md-6">
                <div class="section-head">
                    <h2 data-title="L.P">Lo Nuevo</h2>
                    <p class="mb-0">Click House Inmobiliaria</p>
                </div>
            </div>  
            {{-- <div class="col-md-6">
                <div class="button-group">
                    <a href="#" class="button button-small show-latest-sale active">Venta</a>
                    <a href="#" class="button button-small show-latest-rent">Alquiler</a>
                </div>
            </div> --}}
        </div>
    </div>
    <div class="latest-sale-tab">
        <div class="container-fluid">
            <div class="sale latest-sale">
                
                @foreach ($properties['latest6'] as $property)
            <div class="featured-tt">
                <div class="featured-item">
                    <div class="img">
                        <img src="/storage/propiedades/{{$property->img1}}" alt="Clickhouse Inmobiliaria" class="img-fluid">
                        <div class="hover">
                            <a href="/propiedades/{{$property->id}}" class="button-amount">{{$property->moneda()}} {{ number_format($property['precio'], 0 ) }}</a>
                            {{-- @if ($property['estado'] == 1)
                                <a href="/propiedades/{{$property['id']}}" class="button-amount" style="background-color: #757c8c">en Alquiler</a>
                            @else
                                <a href="/propiedades/{{$property['id']}}" class="button-amount" style="background-color: #757c8c">en Venta</a>
                            @endif --}}
                            <div class="features-icon">
                                <a href="/propiedades/{{$property->id}}"><i class="far fa-heart"></i></a>
                                <a href="/propiedades/{{$property->id}}"><i class="fas fa-exchange-alt"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="content">
                        <h4><a href="/propiedades/{{$property->id}}">{{$property->nombre}}</a></h4>
                        <p class="mb-0"><i class="fas fa-map-marker-alt primary-color"></i> {{$property['direccion']}}, {{$property->city->nombre}}</p>
                    </div>
                    <div class="post-footer">
                        <div class="author d-flex">
                            <!-- <div class="img">
                                <img src="https://via.placeholder.com/37/ededed/000/" class="rounded-circle" alt="Clickhouse Inmobiliaria">
                            </div> -->
                            <!-- <p class="mb-0 align-self-center">Lionel Richie</p> -->
                        </div>
                        <div class="post-icons">
                            <ul class="list-inline">
                                <li class="list-inline-item"><i class="flaticon-shower"></i> <span class="room-count">{{$property->habitaciones}}</span></li>
                                <li class="list-inline-item"><i class="flaticon-bed"></i> <span class="bed-count">{{$property->camas}}</span></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach

            </div>
            <div class="latest-sale-dots"></div>
        </div>
    </div>
</section>