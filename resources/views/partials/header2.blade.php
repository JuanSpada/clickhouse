<header id="header" class="header-layout-nine unveiled-navigation">
    <!-- Header Top -->
    <div class="top-bar primary-bg" id="topbar">
        <div class="container">
            <div class="row">
                <div class="col-md-8 align-self-center">
                    <div class="bar-left">
                        <ul class="list-inline d-inline-block">
                            <li class="list-inline-item mr-4"><i class="fas fa-phone-alt mr-1"></i>
                                098 569 931</li>
                            <li class="list-inline-item"><i class="far fa-envelope-open mr-1"></i>
                                info@clickhouseinmobiliaria.com</li>
                        </ul>
                    </div>
                </div>
                <!-- <div class="col-md-4 text-right">
                    <ul class="list-inline">
                        <li class="list-inline-item"> <a href="#">About</a> / <a href="#">Research</a> / <a href="#">Careers</a></li>
                    </ul>
                </div> -->
            </div>
        </div>
    </div>

    <div id="header-bottom">
        <div class="container">
            <div class="menuzord main-navigation" id="menuzord">
                <a href="/" class="menuzord-brand"><img src="/img/logo-recto.png" style="width:206px;height:43px;" alt="Clickhouse Inmobiliaria" data-rjs="3"></a>
                <div class="view-mobile mobile-offcanvas">
                    <a href="/" class="open-canvas off-canvas-btn"><img src="/img/off-canvas.png" alt="Clickhouse Inmobiliaria"></a>
                </div>
                <ul class="menuzord-menu menuzord-right">
                    <li>
                        <a href="/">Inicio</a>
                    </li>
                    <li class="active">
                        <a href="/propiedades">Propiedades</a>
                        <ul class="dropdown">
                            <li><a href="/propiedades?estado=1">Alquiler</a></li>
                            <li><a href="/propiedades?estado=2">Venta</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="/#about">Nosotros</a>
                    </li>
                    <li>
                        <a href="/contacto">Contacto</a>
                    </li>
                    <li class="hide-mobile"><a href="#" class="open-canvas off-canvas-btn"><img src="/img/off-canvas.png" alt="Clickhouse Inmobiliaria"></a></li>
                </ul>
            </div>
        </div>
    </div>
</header>