<!doctype html>
<html lang="en">
    @include('partials.panel-admin-head')
  <body>
    <nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0 shadow">
      <a class="navbar-brand col-md-3 col-lg-2 mr-0 px-3" href="/">Click House Inmobiliaria</a>
      <button class="navbar-toggler position-absolute d-md-none collapsed" type="button" data-toggle="collapse" data-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      {{-- <input class="form-control form-control-dark w-100" type="text" placeholder="Search" aria-label="Search"> --}}
      <ul class="navbar-nav px-3">
        <li class="nav-item text-nowrap">
          <a class="nav-link" href="{{ route('logout') }}"
            onclick="event.preventDefault();
                          document.getElementById('logout-form').submit();">
              {{ __('Logout') }}
          </a>

          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
              @csrf
          </form>
        </li>
      </ul>
    </nav>

    <div class="container-fluid">
      <div class="row">
        <nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
          <div class="sidebar-sticky pt-3">
            <ul class="nav flex-column">
              <li class="nav-item">
                <a class="nav-link" id="linkprop" href="/panel-admin">
                  <span data-feather="home"></span>
                  Propiedades
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="linknewprop" href="/panel-admin/new">
                  <span data-feather="file"></span>
                  Nueva Propiedad
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="linkcategory" href="/panel-admin/categorias">
                  <span data-feather="layers"></span>
                  Categorías
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="linkcity" href="/panel-admin/ciudades">
                  <span data-feather="users"></span>
                  Ciudades
                </a>
              </li>
            </ul>
          </div>
        </nav>
        @yield('content')
      </div>
    </div>
  <footer class="my-5 pt-5 text-muted text-center text-small">
    <p class="mb-1">&copy; 2020 ClickHouse Inmobiliaria</p>
  </footer>
  <script src="https://kit.fontawesome.com/7a75950ec5.js" crossorigin="anonymous"></script>
  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
  <script>window.jQuery || document.write('<script src="/panel/jquery.slim.min.js"><\/script>')</script><script src="/panel/bootstrap.bundle.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.9.0/feather.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.min.js"></script>
  <script src="/panel/dashboard.js"></script></body>
</html>
