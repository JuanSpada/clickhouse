<section id="feature" class="feature-layout-one pb-100">
    <div class="container">
        <div class="row mb-60">
            <div class="col-md-6">
                <div class="section-head">
                    <h2 data-title="N.F.P">Tu futuro hogar te espera</h2>
                    <p class="mb-0">Tenemos la experiencia y el conocimiento profundo del mercado</p>
                </div>
            </div>
            <div class="col-md-6 align-self-center">
                <div class="button-group">
                    <a href="#" class="button button-small show-sale active">Alquiler</a>
                    <a href="#" class="button button-small show-rent">Venta</a>
                </div>
            </div>
        </div>
        <!-- ALQUILER -->
        <div class="sale-tab">
            <div class="sale">
                <div class="row">
                    @foreach ($properties['first6'] as $property)
                        @if ($property['estado'] == 1)
                            <div class="col-md-4">
                                <div class="featured-item">
                                    <div class="img">
                                        <img src="/storage/propiedades/{{$property['img1']}}" alt="Clickhouse Inmobiliaria" class="img-fluid">
                                        <div class="hover">
                                            <a href="/propiedades/{{$property->id}}" class="button-amount">{{$property->moneda()}} {{ number_format($property['precio'], 0 ) }}</a>
                                            <div class="features-icon">
                                                <a href="/propiedades/{{$property->id}}"><i class="far fa-heart"></i></a>
                                                <a href="/propiedades/{{$property->id}}"><i class="fas fa-exchange-alt"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="content">
                                        <h4><a href="/propiedades/{{$property->id}}"> {{$property['nombre']}}</a></h4>
                                        <p class="mb-0"><i class="fas fa-map-marker-alt primary-color"></i> {{$property['direccion']}}, {{$property->city->nombre}}</p>
                                    </div>
                                    <div class="post-footer">
                                        <div class="post-icons">
                                            <ul class="list-inline">
                                                <li class="list-inline-item"><i class="flaticon-shower"></i> <span class="room-count">{{$property['baños']}}</span></li>
                                                <li class="list-inline-item"><i class="flaticon-bed"></i> <span class="bed-count">{{$property['habitaciones']}}</span></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif                    
                    @endforeach
                </div>
            </div>
        </div>
        <!-- VENTA -->
        <div class="rent-tab">
            <div class="rent">
                <div class="row">
                    @foreach ($properties['first6'] as $property)
                        @if ($property['estado'] == 2)
                            <div class="col-md-4">
                                <div class="featured-item">
                                    <div class="img">
                                        <img src="/storage/propiedades/{{$property['img1']}}" width="400" height="350" style="width:400px;height:350px;" alt="Clickhouse Inmobiliaria" class="img-fluid">
                                        <div class="hover">
                                            <a href="/propiedades/{{$property->id}}" class="button-amount">{{$property->moneda()}} {{ number_format($property['precio'], 0 ) }}</a>
                                            <div class="features-icon">
                                                <a href="/propiedades/{{$property->id}}"><i class="far fa-heart"></i></a>
                                                <a href="/propiedades/{{$property->id}}"><i class="fas fa-exchange-alt"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="content">
                                        <h4><a href="/propiedades/{{$property->id}}">${{$property['nombre']}}</a></h4>
                                        <p class="mb-0"><i class="fas fa-map-marker-alt primary-color"></i> {{$property['direccion']}}, {{$property->city->nombre}}</p>
                                    </div>
                                    <div class="post-footer">
                                        <div class="post-icons">
                                            <ul class="list-inline">
                                                <li class="list-inline-item"><i class="flaticon-shower"></i> <span class="room-count">${{$property['baño']}}</span></li>
                                                <li class="list-inline-item"><i class="flaticon-bed"></i> <span class="bed-count">${{$property['habitaciones']}}</span></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>