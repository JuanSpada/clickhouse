<footer class="footer-layout-eight" id="footer">
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="footer-widget">
                        <h4 class="color-white">Ciudades</h4>
                        <ul class="list-footer">
                            @foreach ($citys as $city)
                                <li><a href="/propiedades?city={{$city->id}}">{{$city->nombre}}</a></li>
                            @endforeach
                        </ul>
                    </div>
                </div> 
                <div class="col-md-3">
                    <div class="footer-widget">
                        <h4 class="color-white">Categorías</h4>
                        <ul class="list-footer">
                            @foreach ($categories as $category)
                                <li><a href="/propiedades?categoria={{$category->id}}">{{$category->nombre}}</a></li>
                            @endforeach
                        </ul>
                    </div>
                </div> 
                <div class="col-md-3">
                    <div class="footer-widget">
                        <h4 class="color-white">Sobre Nosotros</h4>
                        <div class="lists">
                            <ul class="list-footer">
                                <li><a href="/inicio">Inicio</a></li>
                                <li><a href="/propiedades?estado=1">Propiedades en Alquiler</a></li>
                                <li><a href="/propiedades?estado=2">Propiedades en Venta</a></li>
                                <li><a href="/#about">Nosotros</a></li>
                                <li><a href="/contacto">Contacto</a></li>
                            </ul>
                            {{-- <ul class="list-footer">
                                <li><a href="/propiedades?city=1">Colón</a></li>
                                <li><a href="/propiedades?city=2">Lezica</a></li>
                                <li><a href="/propiedades?city=3">Nuevo Paris</a></li>
                                <li><a href="/propiedades?city=4">Sayago</a></li>
                                <li><a href="/propiedades?city=5">Peñarol</a></li>
                            </ul> --}}
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="footer-widget">
                        <h4 class="color-white">Ubicación</h4>
                        <div class="footer-form">
                            <p>Subscribite a nuestra Newsletter</p>
                            <form>
                                <input type="email" name="email" placeholder="">
                                <button type="submit">Enviar</button>
                            </form>
                        </div>
                        <div class="map-image">
                            {{-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d209629.2499723747!2d-56.36766386851163!3d-34.819599861493586!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x959f80ffc63bf7d3%3A0x686fbde87255a664!2sMontevideo%2C%20Departamento%20de%20Montevideo%2C%20Uruguay!5e0!3m2!1ses!2sar!4v1595467260684!5m2!1ses!2sar" width="100%" height="150px" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe> --}}
                            <div id="map-footer" style="height: 150px;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <p class="mb-0">Copyright &copy; 2020. Powered by <a href="https://www.wemanagement.com.ar">We Management</a></p>
                </div>
                <div class="col-md-6 text-right">
                    <ul class="list-inline">
                        <li class="list-inline-item"><a href="/contacto">Contactanos</a></li>
                        <!-- <li class="list-inline-item"><a href="#">Terms and Conditions</a></li>
                        <li class="list-inline-item"><a href="#">Contact Us</a></li> -->
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>