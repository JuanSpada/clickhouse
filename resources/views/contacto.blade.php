<!DOCTYPE html>
<html lang="en">

@include('partials.head2')

<body>

    <!-- Back to Top Button -->
    <a id="top-button" href="#"><i class="fas fa-chevron-up"></i></a>
    <!-- End of Back to Top -->

    <!-- Preloader -->
    <div class="preloader-layout">
        <div class="cube-wrapper">
            <div class="cube-folding">
                <span class="leaf1"></span>
                <span class="leaf2"></span>
                <span class="leaf3"></span>
                <span class="leaf4"></span>
            </div>
            <span class="loading" data-name="loading">Cargando</span>
        </div>
    </div>

    <!-- 
    ======================================== 
        Header Section
    ========================================
    -->

    @include('partials.header2')

    <!-- 
    ======================================== 
        Hero Section
    ========================================
    -->
    <section id="hero" class="innar-hero pt-86">
        <div class="container pa-50">
            <div class="row">
                <div class="col-md-8">
                    <h3 class="mb-0">Contacto</h3>
                </div>
                <div class="col-md-4 align-self-center text-right">
                    <ul class="list-inline">
                        <li class="list-inline-item"><a href="/">Inicio</a></li>
                        <li class="list-inline-item"><i class="flaticon-right-chevron"></i></li>
                        <li class="list-inline-item">Contacto</li>
                    </ul>
                </div>
            </div>
        </div>
        <iframe style="width:100%;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d209629.2499723747!2d-56.36766386851163!3d-34.819599861493586!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x959f80ffc63bf7d3%3A0x686fbde87255a664!2sMontevideo%2C%20Departamento%20de%20Montevideo%2C%20Uruguay!5e0!3m2!1ses!2sar!4v1595467260684!5m2!1ses!2sar" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
        {{-- <div class="map-canvas" style="width: 100%; height: 700px;">
        </div> --}}
    </section>

    <!-- 
    ======================================== 
        Contact content Section
    ========================================
    -->
    <section class="contact-innar pa-100">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="contact-item mb-50">
                        <h4>Direccion:</h4>
                        {{-- <p>Nuestra gratificación más grande es acompañar a nuestros clientes en ese proceso tan importante que es sacar el mejor beneficio en la compra, venta o alquiler de inmuebles y que sean ellos nuestros principales promotores y para eso nos preparamos.</p> --}}
                        <div class="contact-lists">
                            {{-- <div class="contact-list d-flex">
                                <div class="icon">
                                    <i class="fas fa-map-marker-alt primary-color"></i>
                                </div>
                                <p class="mb-0">Direccion</p>
                            </div> --}}

                            <div class="contact-list d-flex">
                                <div class="icon">
                                    <i class="fas fa-phone-alt primary-color"></i>
                                </div>
                                <p class="mb-0">098 569 931</p>
                            </div>

                            <div class="contact-list d-flex">
                                <div class="icon">
                                    <i class="fas fa-envelope primary-color"></i>
                                </div>
                                <p class="mb-0">info@clickhouseinmobiliaria.com</p>
                            </div>
                            <div class="contact-list d-flex">
                                <div class="icon">
                                    <i class="fab fa-whatsapp primary-color"></i>
                                </div>
                                <p class="mb-0">098 569 931</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="contact-form">
                        <div class="input-full">
                            <label for="name">Nombre*</label>
                            <input type="text" name="name" id="name" placeholder="">
                        </div>
                        <div class="input-full">
                            <label for="email">Email*</label>
                            <input type="email" name="email" id="email" placeholder="">
                        </div>
                        <div class="input-full">
                            <label for="phone">Teléfono*</label>
                            <input type="text" name="Phone" id="phone" placeholder="">
                        </div>
                        <div class="textarea-full">
                            <label for="message">Mensaje*</label>
                            <textarea name="message" id="message" placeholder=""></textarea>
                        </div>
                        <div class="input-submit">
                            <button type="submit" class="button button-primary button-big">Enviar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <!-- 
    ======================================== 
        Footer Section
    ========================================
    -->
    @include('partials.footer2')

    <!-- Scripts -->
    <script src="lib/jquery-3.4.1.min.js"></script>
    <script src="lib/jquery-ui.min.js"></script>
    <script src="lib/bootstrap/js/popper.min.js"></script>
    <script src="lib/bootstrap/js/bootstrap.min.js"></script>
    <script src="lib/counterup/waypoints.min.js"></script>
    <script src="lib/counterup/jquery.counterup.min.js"></script>
    <script src="lib/fancybox/jquery.fancybox.min.js"></script>
    <script src="lib/Menuzord/js/menuzord.js"></script>
    <script src="lib/isotope/isotope.min.js"></script>
    <script src="lib/aos/aos.js"></script>
    <script src="lib/retina/retina.min.js"></script>
    <script src="lib/slick/slick.min.js"></script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAFxhUVMwiYKz3ii7f3PL1_YYih3fOtg-M"></script>
    <script src="lib/maps/short-map.js"></script>
    <script src="lib/maps/footer-map.js"></script>

    <!-- Custom Scripts -->
    <script src="js/behome.js"></script>
    <script src="js/slider.js"></script>

    <!-- Off Canvas  -->
    @include('partials.panel-contacto')
    <a href="https://api.whatsapp.com/send?text=&phone=%2B598098569931" class="whatsapp" target="_blank"> <i class="fa fa-whatsapp whatsapp-icon"></i></a>

</body>
</html>