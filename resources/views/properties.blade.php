<!DOCTYPE html>
<html lang="en">

@include('partials.head2')

<body>

    <!-- Back to Top Button -->
    <a id="top-button" href="#"><i class="fas fa-chevron-up"></i></a>
    <!-- End of Back to Top -->

    <!-- Preloader -->
    <div class="preloader-layout">
        <div class="cube-wrapper">
            <div class="cube-folding">
                <span class="leaf1"></span>
                <span class="leaf2"></span>
                <span class="leaf3"></span>
                <span class="leaf4"></span>
            </div>
            <span class="loading" data-name="loading">Cargando</span>
        </div>
    </div>

    <!-- 
    ======================================== 
        Header Section
    ========================================
    -->
    @include('partials.header2')

    <!-- 
    ======================================== 
        Hero Section
    ========================================
    -->
    <section id="hero" class="innar-hero pt-86">
        <div class="container pa-50">
            <div class="row">
                <div class="col-md-6">
                    <h3 class="mb-0">Listado de Propiedades</h3>
                </div>
                <div class="col-md-6 align-self-center text-right">
                    <ul class="list-inline">
                        <li class="list-inline-item"><a href="/">Inicio</a></li>
                        <li class="list-inline-item"><i class="flaticon-right-chevron"></i></li>
                        <li class="list-inline-item">Propiedades</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="img">
            <img src="/img/properties.jpg" alt="Clickhouse Inmobiliaria" class="img-fluid">
        </div>
    </section>

    <div class="property-main-content property-grid property-list pa-100">
        <div class="container">
            <div class="row">
                {{-- @dd($property) --}}
                <div class="col-md-4">
                    <div class="property-sidebar">
                        <div class="sidebar-item item-one mb-30">
                            <div class="sidebar-search">
                                <h4>Búsqueda Avanzada</h4>
                                <form action="" method="GET" id="filtro" name="filtro">
                                    <div class="select-option select-full">
                                        <select name="ciudad">
                                            <option @if($ciudad == null) selected="selected" @endif value="">Elige una Ciudad</option>
                                            @foreach ($citys as $city)
                                                <option @if($ciudad == $city->id) selected="selected" @endif value="{{$city->id}}">{{$city->nombre}}</option>
                                            @endforeach
                                        </select>
                                    
                                    </div>
                                    <div class="select-option select-full">
                                        <select name="categoria">
                                            <option @if($categoria == null) selected="selected" @endif value="">Elige una Categoría</option>
                                            @foreach ($categories as $category)
                                                <option @if($categoria == $category->id) selected="selected" @endif value="{{$category->id}}">{{$category->nombre}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="select-option select-full">
                                        <select name="estado">
                                            <option @if($estado == null) selected="selected" @endif value="">Estado</option>
                                            <option @if($estado == 1) selected="selected" @endif value="1">Alquiler</option>
                                            <option @if($estado == 2) selected="selected" @endif value="2">Venta</option>
                                        </select>
                                    </div>
                                    <div class="select-option select-full">
                                        <select name="habitaciones">
                                            <option @if($habitaciones == null) selected="selected" @endif value="">Habitaciones</option>
                                            <option @if($habitaciones == 1) selected="selected" @endif value="1">1+</option>
                                            <option @if($habitaciones == 2) selected="selected" @endif value="2">2+</option>
                                            <option @if($habitaciones == 3) selected="selected" @endif value="3">3+</option>
                                            <option @if($habitaciones == 4) selected="selected" @endif value="4">4+</option>
                                            <option @if($habitaciones == 5) selected="selected" @endif value="5">5+</option>
                                        </select>
                                    </div>
                                    <!-- <div class="select-option select-full">
                                        <select name="camas">
                                            <option value="">Camas</option>
                                            <option value="1">1+</option>
                                            <option value="2">2+</option>
                                            <option value="3">3+</option>
                                            <option value="4">4+</option>
                                            <option value="5">5+</option>
                                        </select>
                                    </div> -->
                                    <div class="select-option select-full">
                                        <select name="baños">
                                            <option @if($baños == null) selected="selected" @endif value="">Baños</option>
                                            <option @if($baños == 1) selected="selected" @endif value="1">1+</option>
                                            <option @if($baños == 2) selected="selected" @endif value="2">2+</option>
                                            <option @if($baños == 3) selected="selected" @endif value="3">3+</option>
                                            <option @if($baños == 4) selected="selected" @endif value="4">4+</option>
                                            <option @if($baños == 5) selected="selected" @endif value="5">5+</option>
                                        </select>
                                    </div>
                                    <div class="input-submit">
                                        <input type="submit" value="Buscar" class="input-submit">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="property-filter mb-40">
                        <div class="row">
                            <div class="col-3 align-self-center">
                                <p class="mb-0">{{@count($properties)}} propiedades</p>
                            </div>
                            <div class="col-4">
                                <div class="filter-property">
                                    <strong>Estado: </strong>
                                    <div class="dropdown pr-3 d-inline-block">
                                        <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="filter-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                          Default
                                        </a>
                                      
                                        <div class="dropdown-menu" aria-labelledby="filter-btn">
                                          <a class="dropdown-item" href="/propiedades?estado=1">Alquiler</a>
                                          <a class="dropdown-item" href="/propiedades?estado=2">Venta</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-5 text-right align-self-center px-0">
                                <ul class="list-inline">
                                    <li class="list-inline-item"><a href="#" class="show-grid"><i class="fas fa-border-all"></i></a></li>
                                    <li class="list-inline-item"><a href="#" class="show-list active"><i class="fas fa-list-ul"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    
                    <div class="feature-layout-six property-layout-two py-0">
                        <div class="property-gridwise mb-50">
                            <div class="row">
                                @foreach ($properties as $property)
                                    <div class="col-md-6 mb-30">
                                        <div class="feature-item">
                                            <div class="img">
                                                <img src="/storage/propiedades/{{$property['img1']}}" alt="Clickhouse Inmobiliaria" width="390" height="315" style="width:390px;height:315px;" class="img-fluid">
                                                <div class="hover">
                                                    <div class="button-group">
                                                        @if ($property['estado'] == 1)
                                                            <a href="/propiedades/{{$property['id']}}" class="button-blue">Alquiler</a>
                                                        @else
                                                            <a href="/propiedades/{{$property['id']}}" class="tag-primary">Venta</a>
                                                        @endif
                                                        {{-- <a href="#" class="tag-primary">Hot Offer</a> --}}
                                                    </div>
                                                    <div class="pricing">
                                                        <h5 class="mb-0 color-white">${{ number_format($property['precio'], 0 ) }}</h5>
                                                        <a href="#"><i class="fas fa-exchange-alt"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="content-title">
                                                <h4><a href="/propiedades/{{$property['id']}}">{{$property['nombre']}}.</a></h4>
                                                <p><i class="fas fa-map-marker-alt primary-color"></i> {{$property['direccion']}}, {{$property->city->nombre}}</p>
                                            </div>
                                            <div class="content-middle">
                                                <p class="mb-0 d-inline-block"><i class="flaticon-building pr-1"></i> Habitaciones: <strong>{{$property['habitaciones']}}</strong> | Camas: <strong>{{$property['camas']}}</strong> | Baños: <strong>{{$property['baños']}}</strong></p>
                                                <a href="#" class="favorite-feature"><i class="far fa-heart"></i></a>
                                            </div>
                                            {{-- <div class="footer-content">
                                                <div class="author d-flex">
                                                    <div class="img">
                                                        <img src="https://via.placeholder.com/37/ededed/000/" class="rounded-circle" alt="Clickhouse Inmobiliaria">
                                                    </div>
                                                    <p class="mb-0 align-self-center">Lionel Richie</p>
                                                </div>
                                                <div class="right-content">
                                                    <a href="#" class="advanced"><span>a</span></a>
                                                </div>
                                            </div>
                    
                                            <div class="house-feature">
                                                <ul class="list-feature">
                                                    <li>CC Camera</li>
                                                    <li>Wi-Fi</li>
                                                    <li>Power Connections</li>
                                                    <li>Weekley Clean</li>
                                                </ul>
                                            </div> --}}
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            {{ $properties->appends($_GET)->links() }}

                        </div>
                        <div class="property-listwise mb-50">
                            @foreach ($properties as $property)
                            <div class="feature-item mb-30">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="img">
                                            <img src="/storage/propiedades/{{$property['img1']}}" width="405" height="335" style="width:405px;height:335px;" alt="Clickhouse Inmobiliaria" class="img-fluid">
                                            <div class="hover">
                                                <div class="button-group">
                                                    <!-- <a href="/propiedades/{{$property['id']}}" class="button-blue">Oficina</a> -->
                                                    @if ($property['estado'] == 1)
                                                        <a href="/propiedades/{{$property['id']}}" class="button-blue">Alquiler</a>
                                                    @else
                                                        <a href="/propiedades/{{$property['id']}}" class="tag-primary">Venta</a>
                                                    @endif
                                                </div>
                                                <div class="pricing">
                                                    <h5 class="mb-0 color-white">{{$property->moneda()}} {{ number_format($property['precio'], 0 ) }}</h5>
                                                    <a href="/propiedades/{{$property['id']}}"><i class="fas fa-exchange-alt"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 align-self-center mb-30">
                                        <div class="feature-right">
                                            <div class="content-title">
                                                <h4><a href="/propiedades/{{$property['id']}}">{{$property['nombre']}}</a></h4>
                                                <p><i class="fas fa-map-marker-alt primary-color"></i> {{$property['direccion']}}, {{$property->city->nombre}}</p>
                                            </div>
                                            <div class="content-middle">
                                                <p>{{$property['descripcion']}}</p>
                                            </div>
                                            <div class="footer-content">
                                                <p class="mb-0 d-inline-block"><i class="flaticon-building pr-1"></i> Habitaciones: <strong>{{$property['habitaciones']}}</strong> | Camas: <strong>{{$property['camas']}}</strong> | Baños: <strong>{{$property['baños']}}</strong></p>
                                                <a href="/propiedades/{{$property['id']}}" class="favorite-feature"><i class="far fa-heart"></i></a>
                                            </div>
                                            <!-- <div class="footer-content">
                                                <div class="author d-flex">
                                                    <div class="img">
                                                        <img src="https://via.placeholder.com/37/ededed/000/" class="rounded-circle" alt="Clickhouse Inmobiliaria">
                                                    </div>
                                                    <p class="mb-0 align-self-center">Lionel Richie</p>
                                                </div>
                                                <div class="right-content">
                                                    <a href="#" class="advanced"><span>a</span></a>
                                                </div>
                                            </div>
                    
                                            <div class="house-feature">
                                                <ul class="list-feature">
                                                    <li>Wi-Fi</li>
                                                    <li>Aire Aco</li>
                                                    <li>Weekley Clean</li>
                                                </ul>
                                            </div> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            {{ $properties->appends($_GET)->links() }}
                        </div>
                        <!-- <div class="text-center">
                            <a href="#" class="button button-primary button-icon">Load More <i class="fas fa-sync"></i></a>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- 
    ======================================== 
        Footer Section
    ========================================
    -->
    @include('partials.footer2')

    <!-- Scripts -->
    <script src="lib/jquery-3.4.1.min.js"></script>
    <script src="lib/jquery-ui.min.js"></script>
    <script src="lib/bootstrap/js/popper.min.js"></script>
    <script src="lib/bootstrap/js/bootstrap.min.js"></script>
    <script src="lib/counterup/waypoints.min.js"></script>
    <script src="lib/counterup/jquery.counterup.min.js"></script>
    <script src="lib/fancybox/jquery.fancybox.min.js"></script>
    <script src="lib/Menuzord/js/menuzord.js"></script>
    <script src="lib/isotope/isotope.min.js"></script>
    <script src="lib/aos/aos.js"></script>
    <script src="lib/retina/retina.min.js"></script>
    <script src="lib/slick/slick.min.js"></script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAFxhUVMwiYKz3ii7f3PL1_YYih3fOtg-M"></script>
    <script src="lib/maps/footer-map.js"></script>

    <!-- Custom Scripts -->
    <script src="js/behome.js"></script>
    <script src="js/slider.js"></script>

    <!-- Off Canvas  -->
    @include('partials.panel-contacto')
    <a href="https://api.whatsapp.com/send?text=&phone=%2B598098569931" class="whatsapp" target="_blank"> <i class="fa fa-whatsapp whatsapp-icon"></i></a>
    
</body>
</html>