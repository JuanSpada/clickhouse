@extends('partials.panel-admin')
@section('content')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
	  <h1 class="h2">Administrar Ciudades</h1>
    </div>

    {{-- <canvas class="my-4 w-100" id="myChart" width="900" height="380"></canvas> --}}

    	<section class="row d-flex justify-content-center">
		<div class="col-md-8">
			<div class="row">
				<div class="col-6">
					<p>Categrías:</p>

				</div>
				<div class="col-6">
					<!-- Button trigger modal -->
					<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
						Nueva Ciudad
					</button>
				</div>
			</div>
			<table class="table table-striped">
				<thead>
				<tr>
					<th scope="col">ID</th>
					<th scope="col">Nombre</th>
					<th scope="col">Administrar</th>
					<th scope="col">Principal (4 max)</th>
					{{-- <th scope="col">Imagen Principal</th> --}}
				</tr>
				</thead>
				<tbody>
					@foreach ($citys as $city)
						<tr id="citys">
							<th scope="row">{{$city->id}}</th>
							<td class="">{{$city->nombre}}</td>
							<td class="row">
							<a onclick="cityEdit({{$city->id}}, '{{$city->nombre}}')" ><i style="color:#007bff" class="fas fa-edit m-2"></i></a>
							<a href="{{route('removecity', $city->id)}}"><i class="fas fa-trash m-2"></i></a>
							</td>
							<td>
								@if ($city->principal != null)
									<a href="#" onclick="principalEdit({{$city->id}}, null)"><i class="fas fa-check-square" style="color:#007bff;"></i></a>
								@elseif($city->cantidadPrincipales() >= 4)
										<i class="fas fa-square" style=""></i>
								@elseif($city->cantidadPrincipales() <= 4)
										<a href="#" onclick="principalEdit({{$city->id}}, 1)"><i class="fas fa-square" style="color:#007bff;"></i></a>
								@endif
							</td>
							{{-- <td>
								
								<form action="" class="img-save" name="city_img" onsubmit="addImage()" enctype="multipart/form-data">
									@csrf
									<div class="row">

										<div class="col-md-6">
											<input type="text" value="{{$city->id}}" name="city_id" id="city_id" hidden>
											<input type="file" class="form-control-file" id="img" name="img" required>
										</div>
										<div class="col-md-6">
											<button class="" style="background-color:none; border:none; color:#007bff;" type="submit"><i class="far fa-save"></i></button>
										</div>
									</div>
								</form>
							</td> --}}
						</tr>
						@endforeach
				</tbody>
			</table>
		</div>
    	</section>

  	<!-- Modal  new caategory-->
	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form action="{{route ('newcity')}}" method="POST" name="newcity" id="newcity" enctype="multipart/form-data">
					@csrf
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Cargar Ciudad</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<input type="text" required name="nombre" id="nombre" class="form-control mb-2">
						<input type="file" name="img" id="img" class="form-control-file">
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
						<button type="submit" class="btn btn-primary">Cargar</button>
					</div>
				</form>
			</div>
		</div>
	</div>


	{{-- modal edit city --}}

	<div class="modal fade" id="cityEdit" tabindex="-1" role="dialog" aria-labelledby="cityEditLabel" aria-hidden="true" >
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form action="" method="POST" name="editcity" id="editcity" enctype="multipart/form-data">
					@csrf
					<div class="modal-header">
					<h5 class="modal-title" id="cityEditLabel">Editar Ciudad</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
					</div>
					<div class="modal-body">
						<input type="text" name="editId" id="editId" hidden class="form-control" value="">
						<input type="text" name="editNombre" id="editNombre" class="form-control mb-2" value="">
						<input type="file" name="editImg" id="editImg" class="form-control-file">
					</div>
					<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
					<button type="submit" class="btn btn-primary">Cargar</button>
					</div>
				</form>
			</div>
		</div>
	</div>

</main>

<script>

	var element = document.getElementById('linkcity')
	element.classList.add("active");
	  

	// CREANDO CIUDAD
	const form = document.getElementById('newcity');

	form.addEventListener('submit', function(e) {
		e.preventDefault();
		const formData = new FormData(this);
		fetch('/panel-admin/ciudades', {
			method: 'post',
			body: formData
		}).then(function(response){
			return response.text();
		})
		$('#exampleModal').modal('hide');
		location.reload();
	})

	//EDITANDO CIUDAD

	function cityEdit(id, nombre){
		$('#cityEdit').modal('show');
		const editNombre = document.getElementById('editNombre').value = nombre
		const editId = document.getElementById('editId').value = id
		var action = '/panel-admin/ciudades/edit/'+id
		document.getElementById("editcity").action = action;
	}

	const formEdit = document.getElementById('editcity');
	formEdit.addEventListener('submit', function(e) {
		e.preventDefault();
		const formData2 = new FormData(this);
		fetch('/panel-admin/ciudades/edit/' + editId, {
			method: 'post',
			body: formData2
		}).then(function(response){
			return response.text();
		})
		$('#cityEdit').modal('hide');
		location.reload();
	})
		
	// cambiando principales

	function principalEdit(id, principal){
		var data = 
		{
			id: id, 
			principal: principal
		}
		var csrfToken = window.CSRF_TOKEN = '{{ csrf_token() }}';
		var headers = new Headers({
        'X-CSRF-TOKEN': csrfToken,
		'Content-Type': 'application/json'
    	});
		fetch('/panel-admin/ciudades/edit/principal/' + id, {
				method: 'post',
				headers,
				body: JSON.stringify(data),
		}).then(function(response){
			return response.text();
		})
		location.reload();
	}

	// // CARGANDO IMAGEN DE CIUDAD
	// var forms = document.getElementsByClassName("img-save");
	// console.log(forms)

	// function addImage(city_id, img){
	// 	console.log('asdasdd')
	// }

	// // const city_img = document.getElementById('city_img');
	// // city_img.addEventListener('submit', function(e) {
	// // 	console.log('paso')
	// // 	e.preventDefault();
	// // 	$city_id = document.getElementById("city_id").value;
	// // 	const formData = new FormData(this);
	// // 	fetch('/panel-admin/ciudades/img/' + city_id, {
	// // 		method: 'post',
	// // 		body: formData
	// // 	}).then(function(response){
	// // 		return response.text();
	// // 	})
	// // 	// location.reload();
	// // })
	
</script>

@endsection
