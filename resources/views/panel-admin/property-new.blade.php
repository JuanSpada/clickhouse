@extends('partials.panel-admin')
@section('content')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Administrar Propiedades</h1>
    </div>

    {{-- <canvas class="my-4 w-100" id="myChart" width="900" height="380"></canvas> --}}

    <h2>Cargar Propiedad</h2>
    <div class="py-5 text-center">
        {{-- <img class="d-block mx-auto mb-4" src="../assets/brand/bootstrap-solid.svg" alt="" width="72" height="72"> --}}
        {{-- <p class="lead">Below is an example form built entirely with Bootstrap’s form controls. Each required form group has a validation state that can be triggered by attempting to submit the form without completing it.</p> --}}
        </div>
        
        <div class="row d-flex justify-content-center">

        <div class="col-md-10 order-md-1">
            <h4 class="mb-3"></h4>
            <form class="needs-validation" method="POST" action="{{route('storeproperty')}}" enctype="multipart/form-data">
                @csrf
            <div class="row">
                <div class="col-md-6 mb-3">
                <label for="nombre">Nombre</label>
                <input required @error('nombre') is-invalid @enderror type="text" class="form-control" id="nombre" name="nombre" placeholder="" value="">
                </div>
                <div class="col-md-6 mb-3">
                    <label for="direccion">Dirección</label>
                    <input required type="text" class="form-control" id="direccion" name="direccion" placeholder="" value="">
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-12 mb-3">
                    <label for="descripcion">Descripción</label>
                    <textarea required @error('descripcion') is-invalid @enderror class="form-control" id="descripcion" name="descripcion" rows="3"></textarea>
                </div>
            </div>

            <div class="row">

                <div class="col-md-4 mb-3">
                    <label for="ciudad">Ciudad</label>
                    <select required  @error('ciudad') is-invalid @enderror class="custom-select d-block w-100" id="ciudad" name="ciudad">
                        @foreach ($citys as $city)
                            <option value="{{$city->id}}">{{$city->nombre}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="col-md-4 mb-3">
                    <label for="category">Categoría</label>
                    <select required  @error('category') is-invalid @enderror class="custom-select d-block w-100" id="category" name="category">
                        @foreach ($categories as $category)
                        <option value="{{$category->id}}">{{$category->nombre}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="col-md-2 mb-3">
                    <label for="precio">Precio</label>
                    <div class="input-group mb-2">
                        <div class="input-group-prepend">
                          <div class="input-group-text">$</div>
                        </div>
                        <input required  @error('precio') is-invalid @enderror type="text" class="form-control" id="precio" name="precio" placeholder="Precio">
                      </div>
                </div>

                <div class="col-md-2 mb-3">
                    <label for="moneda">Moneda</label>
                    <div class="input-group mb-2">
                        <select required @error('moneda') is-invalid @enderror class="custom-select d-block w-100" id="moneda" name="moneda">
                            <option value="">Seleccionar</option>
                            <option value="1">UY</option>
                            <option value="2">USD</option>
                        </select>
                    </div>

                </div>
                
            </div>
        
            <div class="row">
                <div class="col-md-3 mb-3">
                    <label for="estacionamientos">Estacionamientos</label>
                    <input required  @error('estacionamientos') is-invalid @enderror type="number" class="form-control" name="estacionamientos" id="estacionamientos" placeholder="">
                </div>
                <div class="col-md-3 mb-3">
                    <label for="camas">Camas</label>
                    <input required @error('camas') is-invalid @enderror type="number" class="form-control" id="camas" name="camas" placeholder="">
                </div>
                <div class="col-md-3 mb-3">
                    <label for="baños">Baños</label>
                    <input required @error('baños') is-invalid @enderror type="number" class="form-control" id="baños" name="baños" placeholder="">
                </div>
                <div class="col-md-3 mb-3">
                    <label for="habitaciones">Habitaciones</label>
                    <input required @error('habitaciones') is-invalid @enderror type="number" class="form-control" id="habitaciones" name="habitaciones" placeholder="">
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 mb-3">
                    <label for="living">Living</label>
                    <select required @error('living') is-invalid @enderror class="custom-select d-block w-100" id="living" name="living">
                        <option value="">Seleccionar</option>
                        <option value="1">Si</option>
                        <option value="2">No</option>
                    </select>
                </div>
                <div class="col-md-3 mb-3">
                    <label for="cocina">Cocina</label>
                    <select required @error('cocina') is-invalid @enderror class="custom-select d-block w-100" id="cocina" name="cocina">
                        <option value="">Seleccionar</option>
                        <option value="1">Si</option>
                        <option value="2">No</option>
                    </select>
                </div>
                <div class="col-md-3 mb-3">
                    <label for="m2">M2</label>
                    <input required @error('m2') is-invalid @enderror type="text" class="form-control" id="m2" name="m2" placeholder="200x200">
                </div>
                <div class="col-md-3 mb-3">
                    <label for="estado">Alquiler o Venta?</label>
                    <select required @error('estado') is-invalid @enderror class="custom-select d-block w-100" id="estado" name="estado">
                        <option value="">Seleccionar</option>
                        <option value="1">Alquiler</option>
                        <option value="2">Venta</option>
                    </select>
                </div>
            </div>
            <hr class="mb-4">
            <div class="row">
                <div class="col-md-2 mb-3">
                    <label for="img1">Imagen1</label>
                    <input required type="file" class="form-control-file" id="img1" name="img1">
                </div>
                <div class="col-md-2 mb-3">
                    <label for="img2">Imagen2</label>
                    <input type="file" class="form-control-file" id="img2" name="img2">
                </div>
                <div class="col-md-2 mb-3">
                    <label for="img3">Imagen3</label>
                    <input type="file" class="form-control-file" id="img3" name="img3">
                </div>
                <div class="col-md-2 mb-3">
                    <label for="img4">Imagen4</label>
                    <input type="file" class="form-control-file" id="img4" name="img4">
                </div>
                <div class="col-md-2 mb-3">
                    <label for="img5">Imagen5</label>
                    <input type="file" class="form-control-file" id="img5" name="img5">
                </div>

            </div>
            <hr>
            <button class="btn btn-primary btn-lg btn-block" type="submit">Agregar Propiedad</button>
            </form>
            @if ($errors->any())
                <div class="alert alert-danger mt-3">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
    </div>
</main>
<script>
    var element = document.getElementById('linknewprop')
	element.classList.add("active");
</script>
@endsection