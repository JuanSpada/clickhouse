@extends('partials.panel-admin')
@section('content')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
      <h1 class="h2">Administrar Propiedades</h1>
  </div>
  
  {{-- <canvas class="my-4 w-100" id="myChart" width="900" height="380"></canvas> --}}
  <div class="row">
    <div class="col-md-6">
        <h2>Propiedades</h2>
    </div>
    <div class="col-md-6">
        <a class="btn btn-primary" href="/panel-admin/new">Cargar Propiedad</a>
    </div>
  </div>
  <div class="table-responsive">
      <table class="table">
            <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Nombre</th>
                <th scope="col">Ciudad</th>
                <th scope="col">Categoria</th>
                <th scope="col">Dirección</th>
                <th scope="col">Administrar</th>
                <th scope="col">Principales (max 3)</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($properties as $property)
                <tr>
                    <th scope="row">{{$property->id}}</th>
                    <td>{{$property->nombre}}</td>
                    <td>{{$property->city->nombre}}</td>
                    <td>{{$property->category->nombre}}</td>
                    <td>{{$property->direccion}}</td>
                    <td class="row">
                        <a href="/propiedades/{{$property->id}}/edit"><i class="fas fa-edit m-2"></i></a>
                        <a href="/propiedades/{{$property->id}}/delete"><i class="fas fa-trash m-2"></i></a>
                    </td>
                    <td>
                        @if ($property->principal != null)
                            <a href="#" onclick="principalEdit({{$property->id}}, null)"><i class="fas fa-check-square" style="color:#007bff;"></i></a>
                        @elseif($property->cantidadPrincipales() >= 3)
                                <i class="fas fa-square" style=""></i>
                        @elseif($property->cantidadPrincipales() <= 3)
                                <a href="#" onclick="principalEdit({{$property->id}}, 1)"><i class="fas fa-square" style="color:#007bff;"></i></a>
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
      </table>
      {{ $properties->appends($_GET)->links() }}
  </div>
</main>
<script>
    var element = document.getElementById('linkprop')
	element.classList.add("active");

    function principalEdit(id, principal){
		var data = 
		{
			id: id, 
			principal: principal
		}
		var csrfToken = window.CSRF_TOKEN = '{{ csrf_token() }}';
		var headers = new Headers({
        'X-CSRF-TOKEN': csrfToken,
		'Content-Type': 'application/json'
    	});
		fetch('/panel-admin/edit/principal/' + id, {
				method: 'post',
				headers,
				body: JSON.stringify(data),
		}).then(function(response){
			return response.text();
		})
		location.reload();
	}
</script>
@endsection