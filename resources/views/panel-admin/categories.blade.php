@extends('partials.panel-admin')
@section('content')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
	  <h1 class="h2">Administrar Categorías</h1>
    </div>

    {{-- <canvas class="my-4 w-100" id="myChart" width="900" height="380"></canvas> --}}

    	<section class="row d-flex justify-content-center">
		<div class="col-md-8">
			<div class="row">
				<div class="col-6">
					<p>Categrías:</p>

				</div>
				<div class="col-6">
					<!-- Button trigger modal -->
					<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
						Nueva Categoría
					</button>
				</div>
			</div>
			<table class="table table-striped">
				<thead>
				<tr>
					<th scope="col">ID</th>
					<th scope="col">Nombre</th>
					<th scope="col">Administrar</th>
				</tr>
				</thead>
				<tbody>
					@foreach ($categories as $category)
						<tr id="categories">
							<th scope="row">{{$category->id}}</th>
							<td class="">{{$category->nombre}}</td>
							<td class="row">
							<a onclick="categoryEdit({{$category->id}}, '{{$category->nombre}}')" ><i style="color:#007bff" class="fas fa-edit m-2"></i></a>
							<a href="{{route('removecategory', $category->id)}}"><i class="fas fa-trash m-2"></i></a>
							{{-- <button onclick="categoryEdit({{$category->id}}, '{{$category->nombre}}')" class="btn btn-primary">Editar</button>
							<form method="GET" action="{{route('removecategory', $category->id)}}">
								<button class="btn btn-danger">Eliminar</button>
							</form> --}}
							</td>
						</tr>
						@endforeach
				</tbody>
			</table>
		</div>
    	</section>

  	<!-- Modal  new caategory-->
	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form action="{{route ('newcategory')}}" method="post" name="newcategory" id="newcategory">
					@csrf
					<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Editar Categoría</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
					</div>
					<div class="modal-body">
					<input type="text" name="nombre" id="nombre" class="form-control">
					</div>
					<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
					<button type="submit" class="btn btn-primary">Cargar</button>
					</div>
				</form>
			</div>
		</div>
	</div>


	{{-- modal edit category --}}

	<div class="modal fade" id="categoryEdit" tabindex="-1" role="dialog" aria-labelledby="categoryEditLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form action="" method="POST" name="editcategory" id="editcategory">
					@csrf
					<div class="modal-header">
					<h5 class="modal-title" id="categoryEditLabel">Cargar Categoría</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
					</div>
					<div class="modal-body">
						<input type="text" name="editNombre" id="editNombre" class="form-control" value="">
						<input type="text" name="editId" id="editId" hidden class="form-control" value="">
					</div>
					<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
					<button type="submit" class="btn btn-primary">Cargar</button>
					</div>
				</form>
			</div>
		</div>
	</div>

</main>

<script>
	var element = document.getElementById('linkcategory')
	element.classList.add("active");
	// CREANDO CATEGORIA
	const form = document.getElementById('newcategory');
	form.addEventListener('submit', function(e) {
		e.preventDefault();
		const formData = new FormData(this);
		fetch('/panel-admin/categorias', {
			method: 'post',
			body: formData
		}).then(function(response){
			return response.text();
		})
		$('#exampleModal').modal('hide');
		location.reload();
	})

	//EDITANDO CATEGORIA

	function categoryEdit(id, nombre){
		console.log('edit category')
		$('#categoryEdit').modal('show');
		const editNombre = document.getElementById('editNombre').value = nombre
		const editId = document.getElementById('editId').value = id
		var action = '/panel-admin/categorias/edit/'+id
		document.getElementById("editcategory").action = action;
	}

	const formEdit = document.getElementById('editcategory');
	formEdit.addEventListener('submit', function(e) {
		e.preventDefault();
		const formData2 = new FormData(this);
		fetch('/panel-admin/categorias/edit/' + editId, {
			method: 'post',
			body: formData2
		}).then(function(response){
			return response.text();
		})
		$('#categoryEdit').modal('hide');
		location.reload();
	})
		
</script>

@endsection