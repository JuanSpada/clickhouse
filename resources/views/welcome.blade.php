<!DOCTYPE html>
<html lang="es">

@include('partials.head')

<body>

    <!-- Back to Top Button -->
    <a id="top-button" href="#"><i class="fas fa-chevron-up"></i></a>
    <!-- End of Back to Top -->

    <!-- Preloader -->
    <div class="preloader-layout">
        <div class="cube-wrapper">
            <div class="cube-folding">
                <span class="leaf1"></span>
                <span class="leaf2"></span>
                <span class="leaf3"></span>
                <span class="leaf4"></span>
            </div>
            <span class="loading" data-name="loading">Cargando</span>
        </div>
    </div>

    <!-- 
    ======================================== 
        Header Section
    ========================================
    -->
    @include('partials.header')
    <!-- 
    ======================================== 
        Hero Section
    ========================================
    -->
    <section id="hero" class="hero-layout-one">
        <div class="container">
            <div class="row">
                <div class="col-md-8 mx-auto">
                    <h1 class="color-white text-center">Click House Inmobiliaria</h1>
                    <!-- <form id="search" class="search-layout-one">
                        <div class="short-version">
                            <div class="select">
                                <select name="search-option">
                                    <option value="1">I'm Buying</option>
                                    <option value="2">I'm Selling</option>
                                    <option value="3">I'm Renting</option>
                                </select>
                            </div>
                            <div class="input">
                                <input type="text" placeholder="Enter Your Property Details" name="q">
                            </div>
                            <div class="view-button">
                                <a href="#" class="show-advance"><i class="fas fa-sliders-h"></i></a>
                            </div>
                            <div class="submit-form">
                                <input type="submit" value="Search" name="submit">
                            </div>
                        </div>
                        <div class="advance-search">
                            <h4>Amenities</h4>
                            <div class="row mb-50">
                                <div class="col-md-4">
                                    <div class="checkbox">
                                        <input type="checkbox" id="balcony" name="balcony">
                                        <label for="balcony">Balcony (5)</label>
                                    </div>
                                    <div class="checkbox">
                                        <input type="checkbox" id="hardwood-flows" name="hardwood-flows">
                                        <label for="hardwood-flows">HardWood Flows (6)</label>
                                    </div>
                                    <div class="checkbox">
                                        <input type="checkbox" id="unit-washer" name="unit-washer">
                                        <label for="unit-washer">Unit Washer/Dryer (2)</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="checkbox">
                                        <input type="checkbox" id="basement" name="basement">
                                        <label for="basement">Basement (3)</label>
                                    </div>
                                    <div class="checkbox">
                                        <input type="checkbox" id="parking" name="parking">
                                        <label for="parking">Onsite Parking (9)</label>
                                    </div>
                                    <div class="checkbox">
                                        <input type="checkbox" id="pets-allowed" name="pets-allowed">
                                        <label for="pets-allowed">Pets Allowd (3)</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="checkbox">
                                        <input type="checkbox" id="garage" name="garage">
                                        <label for="garage">Car Garage (5)</label>
                                    </div>
                                    <div class="checkbox">
                                        <input type="checkbox" id="dishwasher" name="dishwasher">
                                        <label for="dishwasher">Dishwasher (6)</label>
                                    </div>
                                    <div class="checkbox">
                                        <input type="checkbox" id="unit-washer-two" name="unit-washer">
                                        <label for="unit-washer-two">Unit Washer/Dryer (2)</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-30">
                                <div class="col-md-4">
                                    <h6>City</h6>
                                    <div class="select-two select-full">
                                        <select name="city">
                                            <option value="0">Select City</option>
                                            <option value="1">New York</option>
                                            <option value="2">California</option>
                                            <option value="3">New Jersey</option>
                                            <option value="4">Las Vegas</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <h6>Price</h6>
                                    <div class="select-two select-half">
                                        <select name="from">
                                            <option value="0">From</option>
                                            <option value="1">$10, 000</option>
                                            <option value="2">$30, 000</option>
                                            <option value="3">$50, 000</option>
                                            <option value="4">$10, 0000</option>
                                        </select>
                                    </div>
                                    <div class="select-two select-half">
                                        <select name="to">
                                            <option value="0">To</option>
                                            <option value="1">$10, 000</option>
                                            <option value="2">$30, 000</option>
                                            <option value="3">$50, 000</option>
                                            <option value="4">$10, 0000</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <h6>Area (sq ft)</h6>
                                    <div class="select-two select-half">
                                        <select name="area">
                                            <option value="0">1 (sq)</option>
                                            <option value="1">30 (sq)</option>
                                            <option value="2">40 (sq)</option>
                                            <option value="3">60 (sq)</option>
                                            <option value="4">100 (sq)</option>
                                        </select>
                                    </div>
                                    <div class="input-half">
                                        <input type="text" placeholder="$500" name="q">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <h6>Beds</h6>
                                    <div class="select-two select-full">
                                        <select name="beds">
                                            <option value="0">Select Beds</option>
                                            <option value="1">2 Beds</option>
                                            <option value="2">3 Beds</option>
                                            <option value="3">4 Beds</option>
                                            <option value="4">5 Beds</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <h6>Bathrooms</h6>
                                    <div class="select-two select-full">
                                        <select name="bathroom">
                                            <option value="0">Select Bathrooms</option>
                                            <option value="1">1 Bathroom</option>
                                            <option value="2">2 Bathroom</option>
                                            <option value="3">3 Bathroom</option>
                                            <option value="4">4 Bathroom</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <h6>Status</h6>
                                    <div class="select-two select-full">
                                        <select name="status">
                                            <option value="0">Select Status</option>
                                            <option value="1">Sale</option>
                                            <option value="2">Rent</option>
                                            <option value="3">Buy</option>
                                            <option value="4">Available</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form> -->
                </div>
            </div>
        </div>
    </section>

    <!-- 
    ======================================== 
        Category Section
    ========================================
    -->
    <section id="category" class="category-layout-one">
        <div class="container">
            <div class="row">
                @foreach ($citys as $city)
                {{-- @dd($city) --}}
                    @if ($city->principal == 1)
                        <div class="col-md-3">
                            <div class="category-item">
                                <img src="/storage/categorias/{{$city->img}}" alt="Clickhouse Inmobiliaria" class="img-fluid">
                                <div class="hover">
                                    <div class="content">
                                        <h4>{{$city->nombre}}</h4>
                                        <p class="count mb-0">{{count($city->properties)}} Propiedades</p>
                                    </div>
                                    <div class="button-hover">
                                        <a href="/propiedades?ciudad={{$city->id}}" class="button-category">Ver Más</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
    </section>

    <!-- 
    ======================================== 
        Featured Section
    ========================================
    -->

    @include('partials.feature')

    <!-- 
    ======================================== 
        Property Section
    ========================================
    -->
    @include('partials.propertyhome')

    <!-- 
    ======================================== 
        Latest Property Section
    ========================================
    -->
    @include('partials.latestproperty')

    <!-- 
    ======================================== 
        About Section
    ========================================
    -->
    <section id="about" class="about-layout-one pa-100">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="about-left">
                        <div class="about-carousel slider-for">
                            <div class="about-slide">
                                <img src="/img/nosotros.jpg" alt="Clickhouse Inmobiliaria"} class="img-fluid">
                                <!-- <h2>35+ Experience</h2> -->
                            </div>
                            {{-- <div class="about-slide">
                                <img src="/img/nosotros-4.jpg" alt="Clickhouse Inmobiliaria"} class="img-fluid">
                                <!-- <h2>25+ Experience</h2> -->
                            </div>
                            <div class="about-slide">
                                <img src="/img/nosotros-3.jpg" alt="Clickhouse Inmobiliaria"} class="img-fluid">
                                <!-- <h2>15+ Experience</h2> -->
                            </div> --}}
                        </div>
                        {{-- <div class="about-navigation slider-nav">
                            <div class="about-tt">
                                <div class="about-nav">
                                    <img src="/img/nosotros.jpg" width="165" height="116" style="width:165px;height:115px;" alt="Clickhouse Inmobiliaria">
                                </div>
                            </div>
                            <div class="about-tt">
                                <div class="about-nav">
                                    <img src="/img/nosotros-4.jpg" width="165" height="116" style="width:165px;height:115px;" alt="Clickhouse Inmobiliaria">
                                </div>
                            </div>
                            <div class="about-tt">
                                <div class="about-nav">
                                    <img src="/img/nosotros-3.jpg" width="165" height="116" style="width:165px;height:115px;" alt="Clickhouse Inmobiliaria">
                                </div>
                            </div>
                        </div> --}}
                    </div>
                </div>
                <div class="col-md-6 align-self-center">
                    <div class="section-head mb-40">
                        <h2 data-title="AB" class="mb-50">Somos una empresa joven dedicada a la gestión inmobiliaria.</h2>
                        <p class="mb-0">Creemos que las buenas prácticas, el marketing dedicado, la atención personalizada y profesional, hacen el marco perfecto para ser la mejor opción para nuestros clientes. <br> Entendemos que compensamos perfectamente el hecho de ser “jóvenes” en un mercado donde existen muchas empresas de larga data con la frescura y el agiornamiento a los tiempos modernos.</p>
                    </div>
                    <ul class="about-list mb-50">
                        <li>Nos proponemos ser líderes y que nos juzguen por nuestros resultados.</li>
                        <li>Decidimos dejar de lado la oficina y profundizar en el mundo digital, con las garantías de antes pero pensando en el hoy y sobre todo en el mañana.</li>
                        <li>Nuestra gratificación más grande es acompañar a nuestros clientes en ese proceso tan importante que es sacar el mejor beneficio en la compra, venta o alquiler de inmuebles y que sean ellos nuestros principales promotores y para eso nos preparamos.</li>
                        <li>Queremos que nuestros clientes sean nuestros principales promotores y para eso estamos dispuestos </li>
                    </ul>
                    <a href="/propiedades" class="button button-primary button-white">Ver propiedades</a>
                </div>
            </div>
        </div>
    </section>

    <!-- 
    ======================================== 
        Feature Two Section
    ========================================
    -->
    <section id="feature-two" class="feature-layout-two pb-100">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="feature-item">
                        <i class="flaticon-shower"></i>
                        <h4>Experiencia</h4>
                        <p class="mb-0">Somos una empresa tradicional en Uruguay, reconocida por su seriedad y solidez hace más de 50 años.</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="feature-item">
                        <i class="flaticon-bed"></i>
                        <h4>Gestión integral</h4>
                        <p class="mb-0">Ofrecemos una gestión inmobiliaria integral para el alquiler, venta, mantenimiento y administración de la propiedad.</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="feature-item">
                        <i class="flaticon-bike"></i>
                        <h4>Equipo</h4>
                        <p class="mb-0">Contamos con un equipo especializado y multidisciplinario que conjuga la experiencia de la empresa con la constante innovación y renovación de un equipo joven y pujante.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- 
    ======================================== 
        Team Section
    ========================================
    -->
    <!-- <section id="team" class="team-layout-one pa-100">
        <div class="container">
            <div class="row mb-60">
                <div class="col-md-6">
                    <div class="section-head">
                        <h2 data-title="P.A">Property Agents</h2>
                        <p class="mb-0">Proportioned interiors Club over 10,000 square feet</p>
                    </div>
                </div>
                <div class="col-md-6 align-self-center text-right">
                    <div class="team-dots"></div>
                </div>
            </div>
            <div class="team-carousel">
                <div class="team-tt">
                    <div class="team-item">
                        <a href="#" class="tag">Best Seller</a>
                        <div class="img">
                            <img src="https://via.placeholder.com/230/ededed/999/" alt="Clickhouse Inmobiliaria">
                            <div class="icon">
                                <i class="fas fa-star"></i>
                            </div>
                        </div>
                        <div class="designation text-center">
                            <h4><a href="team-single.html">Ander Russel</a></h4>
                            <p class="mb-0">House Seller</p>
                        </div>
                        <ul class="info">
                            <li><i class="fas fa-envelope primary-color"></i> realested@gmail.com</li>
                            <li><i class="fas fa-phone-alt primary-color"></i> +99 586 354 5698</li>
                            <li><i class="fas fa-globe-americas primary-color"></i> www.realested.com</li>
                        </ul>
                    </div>
                </div>

                <div class="team-tt">
                    <div class="team-item">
                        <a href="#" class="tag">Best Seller</a>
                        <div class="img">
                            <img src="https://via.placeholder.com/230/ededed/999/" alt="Clickhouse Inmobiliaria">
                            <div class="icon">
                                <i class="fas fa-star"></i>
                            </div>
                        </div>
                        <div class="designation text-center">
                            <h4><a href="team-single.html">Zakir Hossain</a></h4>
                            <p class="mb-0">House Seller</p>
                        </div>
                        <ul class="info">
                            <li><i class="fas fa-envelope primary-color"></i> realested@gmail.com</li>
                            <li><i class="fas fa-phone-alt primary-color"></i> +99 586 354 5698</li>
                            <li><i class="fas fa-globe-americas primary-color"></i> www.realested.com</li>
                        </ul>
                    </div>
                </div>

                <div class="team-tt">
                    <div class="team-item">
                        <a href="#" class="tag">Best Seller</a>
                        <div class="img">
                            <img src="https://via.placeholder.com/230/ededed/999/" alt="Clickhouse Inmobiliaria">
                            <div class="icon">
                                <i class="fas fa-star"></i>
                            </div>
                        </div>
                        <div class="designation text-center">
                            <h4><a href="team-single.html">Saidul I.</a></h4>
                            <p class="mb-0">House Seller</p>
                        </div>
                        <ul class="info">
                            <li><i class="fas fa-envelope primary-color"></i> realested@gmail.com</li>
                            <li><i class="fas fa-phone-alt primary-color"></i> +99 586 354 5698</li>
                            <li><i class="fas fa-globe-americas primary-color"></i> www.realested.com</li>
                        </ul>
                    </div>
                </div>

                <div class="team-tt">
                    <div class="team-item">
                        <a href="#" class="tag">Best Seller</a>
                        <div class="img">
                            <img src="https://via.placeholder.com/230/ededed/999/" alt="Clickhouse Inmobiliaria">
                            <div class="icon">
                                <i class="fas fa-star"></i>
                            </div>
                        </div>
                        <div class="designation text-center">
                            <h4><a href="team-single.html">Ander Russel</a></h4>
                            <p class="mb-0">House Seller</p>
                        </div>
                        <ul class="info">
                            <li><i class="fas fa-envelope primary-color"></i> realested@gmail.com</li>
                            <li><i class="fas fa-phone-alt primary-color"></i> +99 586 354 5698</li>
                            <li><i class="fas fa-globe-americas primary-color"></i> www.realested.com</li>
                        </ul>
                    </div>
                </div>

                <div class="team-tt">
                    <div class="team-item">
                        <a href="#" class="tag">Best Seller</a>
                        <div class="img">
                            <img src="https://via.placeholder.com/230/ededed/999/" alt="Clickhouse Inmobiliaria">
                            <div class="icon">
                                <i class="fas fa-star"></i>
                            </div>
                        </div>
                        <div class="designation text-center">
                            <h4><a href="team-single.html">Zakir Hossain</a></h4>
                            <p class="mb-0">House Seller</p>
                        </div>
                        <ul class="info">
                            <li><i class="fas fa-envelope primary-color"></i> realested@gmail.com</li>
                            <li><i class="fas fa-phone-alt primary-color"></i> +99 586 354 5698</li>
                            <li><i class="fas fa-globe-americas primary-color"></i> www.realested.com</li>
                        </ul>
                    </div>
                </div>

                <div class="team-tt">
                    <div class="team-item">
                        <a href="#" class="tag">Best Seller</a>
                        <div class="img">
                            <img src="https://via.placeholder.com/230/ededed/999/" alt="Clickhouse Inmobiliaria">
                            <div class="icon">
                                <i class="fas fa-star"></i>
                            </div>
                        </div>
                        <div class="designation text-center">
                            <h4><a href="team-single.html">Saidul I.</a></h4>
                            <p class="mb-0">House Seller</p>
                        </div>
                        <ul class="info">
                            <li><i class="fas fa-envelope primary-color"></i> realested@gmail.com</li>
                            <li><i class="fas fa-phone-alt primary-color"></i> +99 586 354 5698</li>
                            <li><i class="fas fa-globe-americas primary-color"></i> www.realested.com</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section> -->

    <!-- 
    ======================================== 
        Blog Section
    ========================================
    -->
    <!-- <section id="blog" class="blog-layout-one pa-100">
        <div class="container">
            <div class="row mb-60">
                <div class="col-md-6">
                    <div class="section-head">
                        <h2 data-title="L.N">Latest News</h2>
                        <p class="mb-0">Proportioned interiors Club over 10,000 square feet</p>
                    </div>
                </div>
                <div class="col-md-6 align-self-center">
                    <div class="blog-dots"></div>
                </div>
            </div>
            <div class="blog-carousel">
                <div class="blog-tt hover-active">
                    <div class="blog-item">
                        <h5>26 <span>Feb</span></h5>
                        <h4><a href="#">Successful campaigns as usually require a campaign manager</a></h4>
                        <p class="mb-50">Gogether we the people achieve more than any single</p>
                        <a href="#" class="button button-white button-blog">Read More</a>
                    </div>
                </div>

                <div class="blog-tt hover-active">
                    <div class="blog-item active">
                        <h5>26 <span>Feb</span></h5>
                        <h4><a href="#">Propiedades for sale, we curr ently have a 21 day average selling time!</a></h4>
                        <p class="mb-50">Gogether we the people achieve more than any single</p>
                        <a href="#" class="button button-white button-blog">Read More</a>
                    </div>
                </div>

                <div class="blog-tt hover-active">
                    <div class="blog-item">
                        <h5>26 <span>Feb</span></h5>
                        <h4><a href="#"> We currently do usually require a campaign manager complete duty!</a></h4>
                        <p class="mb-50">Gogether we the people achieve more than any single</p>
                        <a href="#" class="button button-white button-blog">Read More</a>
                    </div>
                </div>

                <div class="blog-tt hover-active">
                    <div class="blog-item">
                        <h5>26 <span>Feb</span></h5>
                        <h4><a href="#">Successful campaigns as usually require a campaign manager</a></h4>
                        <p class="mb-50">Gogether we the people achieve more than any single</p>
                        <a href="#" class="button button-white button-blog">Read More</a>
                    </div>
                </div>
            </div>
        </div>
    </section> -->

    <!-- 
    ======================================== 
        Instagram Section
    ========================================
    -->
    <div id="instagram" class="instagram-layout-one">
        <div class="instagram-item">
            <a data-fancybox="Instagram" href="#">
                <img src="img/ig-1.png" alt="Clickhouse Inmobiliaria" class="img-fluid">
            </a>
        </div>
        <div class="instagram-item">
            <a data-fancybox="Instagram" href="#">
                <img src="img/ig-2.png" alt="Clickhouse Inmobiliaria" class="img-fluid">
            </a>
        </div>
        <div class="instagram-item">
            <a data-fancybox="Instagram" href="#">
                <img src="img/ig-3.png" alt="Clickhouse Inmobiliaria" class="img-fluid">
            </a>
        </div>
        <div class="instagram-item">
            <a data-fancybox="Instagram" href="#">
                <img src="img/ig-4.png" alt="Clickhouse Inmobiliaria" class="img-fluid">
            </a>
        </div>
        <div class="instagram-item">
            <a data-fancybox="Instagram" href="#">
                <img src="img/ig-5.png" alt="Clickhouse Inmobiliaria" class="img-fluid">
            </a>
        </div>
    </div>

    <!-- 
    ======================================== 
        Feature three Section
    ========================================
    -->
    <!-- <section id="feature-three" class="feature-layout-three pa-100">
        <div class="container">
            <div class="section-head mb-60">
                <h2 data-title="O.C.F">Our Company Features</h2>
                <p class="mb-0">Proportioned interiors Club over 10,000 square feet</p>
            </div>
            <div class="row text-center">
                <div class="col-md-4">
                    <div class="feature-item">
                        <div class="icon">
                            <i class="flaticon-sketch"></i>
                        </div>
                        <h4>High Quality Design</h4>
                        <p class="mb-0">How often you run your system, the m’s age, its make and model, and whether or not it was serviced</p>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="feature-item">
                        <div class="icon">
                            <i class="flaticon-rent"></i>
                        </div>
                        <h4>Developments</h4>
                        <p class="mb-0">How often you run your system, the m’s age, its make and model, and whether or not it was serviced</p>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="feature-item">
                        <div class="icon">
                            <i class="flaticon-shrink"></i>
                        </div>
                        <h4>Green and Environment</h4>
                        <p class="mb-0">How often you run your system, the m’s age, its make and model, and whether or not it was serviced</p>
                    </div>
                </div>
            </div>
        </div>
    </section> -->

    <!-- 
    ======================================== 
        Testimonial Section
    ========================================
    -->
    <!-- <section id="testimonial" class="testimonial-layout-one pa-100">
        <div class="container">
            <div class="row mb-60">
                <div class="col-md-6">
                    <div class="section-head">
                        <h2 data-title="P.M">What Our Clients Say</h2>
                        <p class="mb-0">Proportioned interiors Club over 10,000 square feet</p>
                    </div>
                </div>
                <div class="col-md-6 align-self-center">
                    <div class="testimonial-dots"></div>
                </div>
            </div>
            <div class="testimonial-slider">
                <div class="testimonial-tt">
                    <div class="testimonial-item">
                        <ul class="list-inline">
                            <li class="list-inline-item"><i class="fas fa-star"></i></li>
                            <li class="list-inline-item"><i class="fas fa-star"></i></li>
                            <li class="list-inline-item"><i class="fas fa-star"></i></li>
                            <li class="list-inline-item"><i class="fas fa-star"></i></li>
                            <li class="list-inline-item"><i class="fas fa-star"></i></li>
                        </ul>
                        <p class="lead">Precious ipsum dolor sit amet, consectetur adipisicing elit, sed do smod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud Precious ipsum dolor sit amet, consectetur adipisicing elit, sed do smod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud</p>
                        <div class="author">
                            <img src="https://via.placeholder.com/80/ededed/000/" alt="Clickhouse Inmobiliaria">
                            <h4>Ander Russel</h4>
                            <p class="mb-0">Buying House</p>
                        </div>
                    </div>
                </div>
                <div class="testimonial-tt">
                    <div class="testimonial-item">
                        <ul class="list-inline">
                            <li class="list-inline-item"><i class="fas fa-star"></i></li>
                            <li class="list-inline-item"><i class="fas fa-star"></i></li>
                            <li class="list-inline-item"><i class="fas fa-star"></i></li>
                            <li class="list-inline-item"><i class="fas fa-star"></i></li>
                            <li class="list-inline-item"><i class="fas fa-star"></i></li>
                        </ul>
                        <p class="lead">Precious ipsum dolor sit amet, consectetur adipisicing elit, sed do smod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud Precious ipsum dolor sit amet, consectetur adipisicing elit, sed do smod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud</p>
                        <div class="author">
                            <img src="https://via.placeholder.com/80/ededed/000/" alt="Clickhouse Inmobiliaria">
                            <h4>Ander Russel</h4>
                            <p class="mb-0">Buying House</p>
                        </div>
                    </div>
                </div>
                <div class="testimonial-tt">
                    <div class="testimonial-item">
                        <ul class="list-inline">
                            <li class="list-inline-item"><i class="fas fa-star"></i></li>
                            <li class="list-inline-item"><i class="fas fa-star"></i></li>
                            <li class="list-inline-item"><i class="fas fa-star"></i></li>
                            <li class="list-inline-item"><i class="fas fa-star"></i></li>
                            <li class="list-inline-item"><i class="fas fa-star"></i></li>
                        </ul>
                        <p class="lead">Precious ipsum dolor sit amet, consectetur adipisicing elit, sed do smod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud Precious ipsum dolor sit amet, consectetur adipisicing elit, sed do smod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud</p>
                        <div class="author">
                            <img src="https://via.placeholder.com/80/ededed/000/" alt="Clickhouse Inmobiliaria">
                            <h4>Ander Russel</h4>
                            <p class="mb-0">Buying House</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section> -->

    <!-- 
    ======================================== 
        Footer Section
    ========================================
    -->
    <footer id="footer" class="footer-layout-one">
        <div class="footer-top">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <div class="footer-widget">
                            <h4>Ciudades</h4>
                            <ul class="list-footer">
                                @foreach ($citys as $city)
                                    <li><a href="/propiedades?city={{$city->id}}">{{$city->nombre}}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="footer-widget">
                            <h4 class="">Categorías</h4>
                            <ul class="list-footer">
                                @foreach ($categories as $category)
                                    <li><a href="/propiedades?categoria={{$category->id}}">{{$category->nombre}}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    </div> 
                    <div class="col-md-3">
                        <div class="footer-widget">
                            <h4>Nosotros</h4>
                            <div class="lists">
                                <ul class="list-footer">
                                    <li><a href="/inicio">Inicio</a></li>
                                    <li><a href="#about">Nosotros</a></li>
                                    <li><a href="/propiedades">Propiedades</a></li>
                                    <li><a href="/contacto">Contacto</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="footer-widget">
                            <h4>Ubicación</h4>
                            <div class="map-image">
                                <div id="map-footer" style="height: 210px;"></div>
                                {{-- <iframe style="width:100%;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d209629.2499723747!2d-56.36766386851163!3d-34.819599861493586!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x959f80ffc63bf7d3%3A0x686fbde87255a664!2sMontevideo%2C%20Departamento%20de%20Montevideo%2C%20Uruguay!5e0!3m2!1ses!2sar!4v1595467260684!5m2!1ses!2sar" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe> --}}

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <p class="mb-0">Copyright &copy; 2020. Powered by <a href="https://www.wemanagement.com.ar">We Management</a></p>
                    </div>
                    <div class="col-md-6 text-right">
                        <ul class="list-inline">
                            <li class="list-inline-item"><a href="/contacto">Contactanos</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <a href="https://api.whatsapp.com/send?text=&phone=%2B598098569931" class="whatsapp" target="_blank"> <i class="fa fa-whatsapp whatsapp-icon"></i></a>
    
    <!-- Scripts -->
    <script src="lib/jquery-3.4.1.min.js"></script>
    <script src="lib/jquery-ui.min.js"></script>
    <script src="lib/bootstrap/js/popper.min.js"></script>
    <script src="lib/bootstrap/js/bootstrap.min.js"></script>
    <script src="lib/counterup/waypoints.min.js"></script>
    <script src="lib/counterup/jquery.counterup.min.js"></script>
    <script src="lib/fancybox/jquery.fancybox.min.js"></script>
    <script src="lib/Menuzord/js/menuzord.js"></script>
    <script src="lib/isotope/isotope.min.js"></script>
    <script src="lib/aos/aos.js"></script>
    <script src="lib/retina/retina.min.js"></script>
    <script src="lib/slick/slick.min.js"></script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAFxhUVMwiYKz3ii7f3PL1_YYih3fOtg-M"></script>
    <script src="lib/maps/footer-map.js"></script>

    <!-- Custom Scripts -->
    <script src="js/behome.js"></script>
    <script src="js/slider.js"></script>
    
</body>
</html>