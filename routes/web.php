<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/', 'PropertyController@index');
Route::get('/propiedades', 'PropertyController@propiedades')->name('propiedades');
Route::get('/contacto', 'HomeController@contacto')->name('contacto');
Route::get('/propiedades/{id}', 'PropertyController@show')->name('propiedad');
Route::get('/artisan/{command}', 'ArtisanController@artisan');

Route::middleware(['auth'])->group(function() {
    Route::get('/panel-admin', 'HomeController@index')->name('home');
    Route::get('/panel-admin/new', 'HomeController@newProperty')->name('newproperty');
    Route::post('/panel-admin/new', 'PropertyController@store')->name('storeproperty');
    Route::get('/propiedades/{id}/delete', 'PropertyController@destroy')->name('destroy');
    Route::get('/propiedades/{id}/edit', 'PropertyController@edit')->name('edit');
    Route::get('/propiedades/{id}/edit/delete/{img}', 'PropertyController@deleteimg')->name('deleteimg');
    Route::post('/propiedades/{id}/edit', 'PropertyController@update')->name('update');
    Route::post('/panel-admin/edit/principal/{id}', 'PropertyController@principal')->name('propertyprincipal');
    Route::get('/panel-admin/categorias', 'CategoryController@index')->name('categorias');
    Route::post('/panel-admin/categorias', 'CategoryController@store')->name('newcategory');
    Route::post('/panel-admin/categorias/edit/{id}', 'CategoryController@edit')->name('editcategory');
    Route::get('/panel-admin/categorias/{id}/delete', 'CategoryController@destroy')->name('removecategory');
    Route::get('/panel-admin/ciudades', 'CityController@index')->name('citys');
    Route::post('/panel-admin/ciudades', 'CityController@store')->name('newcity');
    Route::post('/panel-admin/ciudades/img/{id}', 'CityController@img')->name('imgcity');
    Route::post('/panel-admin/ciudades/edit/{id}', 'CityController@edit')->name('editcity');
    Route::post('/panel-admin/ciudades/edit/principal/{id}', 'CityController@principal')->name('cityprincipal');
    Route::get('/panel-admin/ciudades/{id}/delete', 'CityController@destroy')->name('removecity');
});