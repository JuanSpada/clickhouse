<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    public function properties()
    {
        return $this->hasMany(Property::class);
    }

    public function cantidadPrincipales()
    {
        $principales = City::where('principal', '=' , 1)->get();
        // dd($principales);
        $cantidad = count($principales);
        $principales = $cantidad;
        // dd($cantidad);
        return $principales;
    }
}

/* Ciudades / ID
Colón = 1
Lezica = 2
Nuevo Paris = 3
Sayago = 4
Peñarol = 5  

Principales (hasta 4 principales)
*/

