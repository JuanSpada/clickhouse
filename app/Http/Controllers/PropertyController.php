<?php

namespace App\Http\Controllers;

use App\Property;
use App\Category;
use App\City;
use Illuminate\Http\Request;
use Image;

class PropertyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $properties['latest6'] = Property::orderBy('id', 'desc')->take(6)->get();
        $properties['first6'] = Property::orderBy('id', 'asc')->take(6)->get();
        $allproperties = Property::all();
        $property = Property::latest('created_at')->first();
        $citys = City::all();
        $properties_principales = Property::where('principal', '=', 1)->get();
        $categories = Category::all();
        return view('welcome')
        ->with('properties', $properties)
        ->with('citys', $citys)
        ->with('allproperties', $allproperties)
        ->with('properties_principales', $properties_principales)
        ->with('categories', $categories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'nombre' => 'required',
            'descripcion' => 'required',
            'precio' => 'required',
            'direccion' => 'required',
            'estacionamientos' => 'required',
            'camas' => 'required',
            'baños' => 'required',
            'habitaciones' => 'required',
            'living' => 'required',
            'moneda' => 'required',
            'cocina' => 'required',
            'm2' => 'required',
            'estado' => 'required',
            'ciudad' => 'required',
            'img1' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
            'img2' => 'image|mimes:jpeg,png,jpg,gif,svg',
            'img3' => 'image|mimes:jpeg,png,jpg,gif,svg',
            'img4' => 'image|mimes:jpeg,png,jpg,gif,svg',
            'img5' => 'image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        //save the data to the database
        $property  = new Property;
        $property->nombre = $request->nombre;
        $property->descripcion = $request->descripcion;
        $property->precio = $request->precio;
        $property->direccion = $request->direccion;
        $property->moneda = $request->moneda;
        $property->img1 = $request->img1;
        if($request->img2){
            $property->img2 = $request->img1;
        }
        if($request->img3){
            $property->img3 = $request->img3;
        }
        if($request->img4){
            $property->img4 = $request->img4;
        }
        if($request->img5){
            $property->img5 = $request->img5;
        }
        $property->estacionamientos = $request->estacionamientos;
        $property->camas = $request->camas;
        $property->baños = $request->baños;
        $property->habitaciones = $request->habitaciones;
        $property->living = $request->living;
        $property->cocina = $request->cocina;
        $property->m2 = $request->m2;
        $property->category_id = $request->category;
        $property->estado = $request->estado;
        $property->city_id = $request->ciudad;
        $property->save();

        if($request->hasFile('img1')){
            $img1 = $request->file('img1');
            $filename = $property->id . '-' . 'img1' . '-' . time() . '.' . $img1->getClientOriginalExtension();
            $property->img1 = $filename;
            $img1->move(public_path('/storage/propiedades'), $filename);
            
        }

        if($request->hasFile('img2')){
            $img2 = $request->file('img2');
            $filename = $property->id . '-' . 'img2' . '-' . time() . '.' . $img2->getClientOriginalExtension();
            $property->img2 = $filename;
            $img2->move(public_path('/storage/propiedades'), $filename);
            
        }

        if($request->hasFile('img3')){
            $img3 = $request->file('img3');
            $filename = $property->id . '-' . 'img3' . '-' . time() . '.' . $img3->getClientOriginalExtension();
            $property->img3 = $filename;
            $img3->move(public_path('/storage/propiedades'), $filename);
            
        }

        if($request->hasFile('img4')){
            $img4 = $request->file('img4');
            $filename = $property->id . '-' . 'img4' . '-' . time() . '.' . $img4->getClientOriginalExtension();
            $property->img4 = $filename;
            $img4->move(public_path('/storage/propiedades'), $filename);
            
        }

        if($request->hasFile('img5')){
            $img5 = $request->file('img5');
            $filename = $property->id . '-' . 'img5' . '-' . time() . '.' . $img5->getClientOriginalExtension();
            $property->img5 = $filename;
            $img5->move(public_path('/storage/propiedades'), $filename);
            
        }
        $property->save();

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Property  $property
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $property = Property::find($id);
        $citys = City::all();
        $categories = Category::all();
        return view('property')->with('property', $property)->with('citys', $citys)->with('categories', $categories);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Property  $property
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = Category::all();
        $citys = City::all();
        $property = Property::find($id);
        return view('panel-admin.property-edit')->with('property', $property)->with('categories', $categories)->with('citys', $citys);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Property  $property
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->moneda);

        $validatedData = $request->validate([
            'nombre' => 'required',
            'descripcion' => 'required',
            'precio' => 'required',
            'direccion' => 'required',
            'estacionamientos' => 'required',
            'camas' => 'required',
            'moneda' => 'required',
            'baños' => 'required',
            'habitaciones' => 'required',
            'living' => 'required',
            'cocina' => 'required',
            'm2' => 'required',
            'estado' => 'required',
            'ciudad' => 'required',
            'img1' => 'image|mimes:jpeg,png,jpg,gif,svg',
            'img2' => 'image|mimes:jpeg,png,jpg,gif,svg',
            'img3' => 'image|mimes:jpeg,png,jpg,gif,svg',
            'img4' => 'image|mimes:jpeg,png,jpg,gif,svg',
            'img5' => 'image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        
        //save the data to the database
        $property  = Property::find($id);
        $property->nombre = $request->nombre;
        $property->moneda = $request->moneda;
        $property->descripcion = $request->descripcion;
        $property->precio = $request->precio;
        $property->direccion = $request->direccion;
        if ($request->img1) {
            $property->img1 = $request->img1;
        }
        if($request->img2){
            $property->img2 = $request->img1;
        }
        if($request->img3){
            $property->img3 = $request->img3;
        }
        if($request->img4){
            $property->img4 = $request->img4;
        }
        if($request->img5){
            $property->img5 = $request->img5;
        }
        $property->estacionamientos = $request->estacionamientos;
        $property->camas = $request->camas;
        $property->baños = $request->baños;
        $property->habitaciones = $request->habitaciones;
        $property->living = $request->living;
        $property->cocina = $request->cocina;
        $property->m2 = $request->m2;
        $property->estado = $request->estado;
        $property->city_id = $request->ciudad;
        $property->category_id = $request->category;
        $property->save();

        if($request->hasFile('img1')){
            $img1 = $request->file('img1');
            $filename = $property->id . '-' . 'img1' . '-' . time() . '.' . $img1->getClientOriginalExtension();
            $property->img1 = $filename;
            $img1->move(public_path('/storage/propiedades'), $filename);
            
        }

        if($request->hasFile('img2')){
            $img2 = $request->file('img2');
            $filename = $property->id . '-' . 'img2' . '-' . time() . '.' . $img2->getClientOriginalExtension();
            $property->img2 = $filename;
            $img2->move(public_path('/storage/propiedades'), $filename);
            
        }

        if($request->hasFile('img3')){
            $img3 = $request->file('img3');
            $filename = $property->id . '-' . 'img3' . '-' . time() . '.' . $img3->getClientOriginalExtension();
            $property->img3 = $filename;
            $img3->move(public_path('/storage/propiedades'), $filename);
            
        }

        if($request->hasFile('img4')){
            $img4 = $request->file('img4');
            $filename = $property->id . '-' . 'img4' . '-' . time() . '.' . $img4->getClientOriginalExtension();
            $property->img4 = $filename;
            $img4->move(public_path('/storage/propiedades'), $filename);
            
        }

        if($request->hasFile('img5')){
            $img5 = $request->file('img5');
            $filename = $property->id . '-' . 'img5' . '-' . time() . '.' . $img5->getClientOriginalExtension();
            $property->img5 = $filename;
            $img5->move(public_path('/storage/propiedades'), $filename);
            
        }
        $property->save();
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Property  $property
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   
        
        $property = Property::find($id);
        $property->delete();
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Property  $property
     * @return \Illuminate\Http\Response
     */
     public function propiedades(Request $request)
    {
        $citys = City::all();
        $categories = Category::all();

        $ciudad = $request->get('ciudad');
        $habitaciones = $request->get('habitaciones');
        $categoria = $request->get('categoria');
        $baños = $request->get('baños');
        $estado = $request->get('estado');

        $properties = Property::orderBy('created_at', 'ASC')
            ->ciudad($ciudad)
            ->categoria($categoria)
            ->habitaciones($habitaciones)
            ->baños($baños)
            ->estado($estado)
            ->paginate(6);
        return view('properties')
        ->with('properties', $properties)
        ->with('ciudad', $ciudad)
        ->with('categoria', $categoria)
        ->with('habitaciones', $habitaciones)
        ->with('baños', $baños)
        ->with('estado', $estado)
        ->with('citys', $citys)
        ->with('categories', $categories);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Property  $property
     * @return \Illuminate\Http\Response
     */
     public function deleteimg($id, $img)
    {
        $property = Property::find($id);

        if ($img == 'img2') {
            $property->img2 = null;
            $property->save();
        }

        if ($img == 'img3') {
            $property->img3 = null;
            $property->save();
        }
        if ($img == 'img4') {
            $property->img4 = null;
            $property->save();
        }
        if ($img == 'img5') {
            $property->img5 = null;
            $property->save();
        }

        return back();
    }
    
     public function principal(Request $request)
     {
        $property_id = $request->id;
        $property = Property::find($property_id);
        // dd($property);
        $property->principal = $request->principal;
        $property->save();
    }

}
