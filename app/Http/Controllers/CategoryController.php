<?php

namespace App\Http\Controllers;

use App\Category;
use App\Property;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();
        return view('panel-admin.categories')->with('categories', $categories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $category = new Category;
        $category->nombre = $request->nombre;
        $category->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        
        // dd($request);
        // error_log($request->editNombre);
        $editId = $request->editId;
        $nombre = $request->editNombre;
        $category = Category::find($editId);
        $category->nombre = $nombre;
        $category->save();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //QUE PREGUNTE A DONDE QUIEREN CAMBIAR DE CATEGORIA A LAS PROPIEDADES QUE ESTEN EN ESTA CATEGORIA
        // $properties = Property::all();
        // dd($properties);

        // foreach ($properties as $property) 
        // {
        //     if($property->category_id == $request_id)
        //     {
                
        //     }
        // }
        $category = Category::find($request->id);
        $category->delete();
        return back();
    }
}
