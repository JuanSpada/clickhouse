<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Property;
use App\Category;
use App\City;
use Artisan;
class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $properties = Property::orderBy('principal', 'DESC')->paginate(10);
        return view('panel-admin.panel')->with('properties', $properties);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function contacto()
    {
        $categories = Category::all();
        $citys = City::all();
        return view('contacto')->with('citys', $citys)->with('categories', $categories);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
     public function newProperty()
    {
        $categories = Category::all();
        $citys = City::all();
        return view('panel-admin.property-new')->with('categories', $categories)->with('citys', $citys);
    }

}
