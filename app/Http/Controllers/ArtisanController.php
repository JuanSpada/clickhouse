<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Artisan;

class ArtisanController extends Controller
{
    public function artisan($command){
        Artisan::call($command);
    }
}
