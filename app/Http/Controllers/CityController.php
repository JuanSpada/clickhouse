<?php

namespace App\Http\Controllers;

use App\City;
use Illuminate\Http\Request;

class CityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $citys = City::orderBy('principal', 'DESC')->get();

        return view('panel-admin.citys')->with('citys', $citys);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $city = new City;
        $city->nombre = $request->nombre;
        $city->save();
        if($request->hasFile('img')){
            $img = $request->file('img');
            $filename = $city->id . '-' . 'img' . '-' . time() . '.' . $img->getClientOriginalExtension();
            $city->img = $filename;
            $img->move(public_path('/storage/categorias'), $filename);
            $city->save();
        }
        // return view('')
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\City  $city
     * @return \Illuminate\Http\Response
     */
    public function show(City $city)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\City  $city
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $editId = $request->editId;
        $nombre = $request->editNombre;
        $city = City::find($request->editId);
        $city->nombre = $nombre;
        $city->save();
        if($request->hasFile('editImg')){
            $img = $request->file('editImg');
            $filename = $city->id . '-' . 'img' . '-' . time() . '.' . $img->getClientOriginalExtension();
            $city->img = $filename;
            $img->move(public_path('/storage/categorias'), $filename);
            $city->save();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\City  $city
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, City $city)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\City  $city
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //QUE PREGUNTE A DONDE QUIEREN CAMBIAR DE CATEGORIA A LAS PROPIEDADES QUE ESTEN EN ESTA CATEGORIA
        $city = City::find($request->id);
        $city->delete();
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\City  $city
     * @return \Illuminate\Http\Response
     */
     public function principal(Request $request)
     {
        $city_id = $request->id;
        $city = City::find($city_id);
        // dd($city);
        $city->principal = $request->principal;
        $city->save();
    }

    public function img(Request $request)
    {
        // dd($request);
        $city = City::find($request->city_id);
        if($request->hasFile('img')){
            $img = $request->file('img');
            $filename = $city->id . '-' . 'img' . '-' . time() . '.' . $img->getClientOriginalExtension();
            $city->img = $filename;
            $img->move(public_path('/storage/categorias'), $filename);
            $city->save();
        }
    }
}
