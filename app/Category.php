<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public $fillable = [
        'nombre',
    ];

    public function properties()
    {
        return $this->hasMany(Property::class);
    }
}
