<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    protected $fillable = [
        'nombre', 'descripcion', 'precio', 'direccion', 'categoria', 'img5', 'img1', 'img2', 'img3', 'img4', 'estacionamientos', 'camas', 'baños', 'habitaciones', 'living', 'cocina', 'm2', 'estado', 'ciudad',
    ];

    //QUERY SCOPE

    public function scopeCiudad($query, $ciudad)
    {   
        if ($ciudad){
            return ($query->where('city_id', $ciudad));
        }
    }

    public function scopeCategoria($query, $categoria)
    {   
        if ($categoria){
            return ($query->where('category_id', $categoria));
        }
    }

    public function scopeEstado($query, $estado)
    {   
        if ($estado){
            return ($query->where('estado', $estado));
        }
    }

    public function scopeHabitaciones($query, $habitaciones)
    {   
        if ($habitaciones){
            return ($query->where('habitaciones', '>=', $habitaciones));
        }
    }

    public function scopeBaños($query, $baños)
    {   
        if ($baños){
            return ($query->where('baños', '>=', $baños));
        }
    }

    public function scopeEstacionamiento($query, $estacionamiento)
    {   
        if ($estacionamiento){
            return ($query->where('estacionamiento', $estacionamiento));
        }
    }


    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }
    
    public function cantidadPrincipales()
    {
        $principales = Property::where('principal', '=' , 1)->get();
        // dd($principales);
        $cantidad = count($principales);
        $principales = $cantidad;
        // dd($cantidad);
        return $principales;
    }

    public function moneda()
    {
        $moneda = $this->moneda;
        switch ($moneda) {
            case 1:
                return '$UY';
                break;
            
            case 2:
                return 'USD';
                break;
        }
        switch ($variable) {
            case 'value':
                # code...
                break;
            
            default:
                # code...
                break;
        }
    }

}




/* 
Estado / ID
Alquiler = 1 
Venta = 2
Principales (hasta 3 principales)
MONEDAS:
1 = UY
2 = USD
*/