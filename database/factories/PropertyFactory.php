<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Property;
use Faker\Generator as Faker;

$factory->define(Property::class, function (Faker $faker) {
    return [
        'nombre' => 'Nombre Propiedad Ejemplo',
        'descripcion' => $faker->realText,
        'precio' => random_int(25000, 900000),
        'direccion' => $faker->streetAddress,
        'category_id' => random_int(1, 4),
        'city_id' => random_int(1, 4),
        'img1' => '1.png',
        'img2' => '2.png',
        'img3' => '3.png',
        'img4' => '4.png',
        'estacionamientos' => random_int(0, 9),
        'camas' => random_int(1, 9),
        'baños' => random_int(1, 6),
        'estado' => random_int(1, 2),
        'habitaciones' => random_int(0, 9),
        'living' => random_int(1, 2),
        'moneda' => random_int(1, 2),
        'cocina' => random_int(1, 2),
        'm2' => random_int(200, 400),
        'estado' => random_int(1, 2),
        'principal' => null,
    ];
});