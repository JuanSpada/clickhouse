<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $user = factory(App\User::class, 1)->create();
        // $propiedades = factory(App\Property::class, 40)->create();
        // $city = factory(App\City::class, 10)->create();
        // $category = factory(App\Category::class, 3)->create();

        DB::table('cities')->insert([
            'nombre' => 'Colón',
            'img' => '1.png',
            'principal' => 1
        ]);
        
        DB::table('cities')->insert([
            'nombre' => 'Lezica',
            'img' => '2.png',
            'principal' => 1
        ]);

        DB::table('cities')->insert([
            'nombre' => 'Nuevo Paris',
            'img' => '3.png',
            'principal' => 1
        ]);

        DB::table('cities')->insert([
            'nombre' => 'Sayago',
            'img' => '4.png',
            'principal' => 1
        ]);

        DB::table('cities')->insert([
            'nombre' => 'Peñarol',
            'img' => '5.png',
            'principal' => null,
        ]);
        
        DB::table('properties')->insert([
            'created_at' => now(),
            'nombre' => 'Propiedad Principal 1',
            'descripcion' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vitae eros quis massa pulvinar suscipit. Proin porttitor dolor nec tellus varius congue sed et nibh. Etiam et nibh eget sapien euismod dapibus. Quisque laoreet porttitor sem, ula bibendum.',
            'precio' => random_int(25000, 900000),
            'direccion' => 'Dirección de ejemplo',
            'category_id' => random_int(1, 4),
            'city_id' => random_int(1, 4),
            'img1' => '1.png',
            'img2' => '2.png',
            'img3' => '3.png',
            'img4' => '4.png',
            'estacionamientos' => random_int(0, 9),
            'camas' => random_int(1, 9),
            'baños' => random_int(1, 6),
            'estado' => random_int(1, 2),
            'habitaciones' => random_int(0, 9),
            'living' => random_int(1, 2),
            'moneda' => random_int(1, 2),
            'cocina' => random_int(1, 2),
            'm2' => random_int(200, 400),
            'estado' => random_int(1, 2),
            'principal' => 1,
        ]);

        DB::table('properties')->insert([
            'created_at' => now(),
            'nombre' => 'Propiedad Principal 2',
            'descripcion' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vitae eros quis massa pulvinar suscipit. Proin porttitor dolor nec tellus varius congue sed et nibh. Etiam et nibh eget sapien euismod dapibus. Quisque laoreet porttitor sem, ula bibendum.',
            'precio' => random_int(25000, 900000),
            'direccion' => 'Dirección de ejemplo',
            'category_id' => random_int(1, 4),
            'city_id' => random_int(1, 4),
            'img1' => '1.png',
            'img2' => '2.png',
            'img3' => '3.png',
            'img4' => '4.png',
            'estacionamientos' => random_int(0, 9),
            'camas' => random_int(1, 9),
            'baños' => random_int(1, 6),
            'estado' => random_int(1, 2),
            'habitaciones' => random_int(0, 9),
            'living' => random_int(1, 2),
            'moneda' => random_int(1, 2),
            'cocina' => random_int(1, 2),
            'm2' => random_int(200, 400),
            'estado' => random_int(1, 2),
            'principal' => 1,
        ]);

        DB::table('properties')->insert([
            'created_at' => now(),
            'nombre' => 'Propiedad Principal 3',
            'descripcion' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vitae eros quis massa pulvinar suscipit. Proin porttitor dolor nec tellus varius congue sed et nibh. Etiam et nibh eget sapien euismod dapibus. Quisque laoreet porttitor sem, ula bibendum.',
            'precio' => random_int(25000, 900000),
            'direccion' => 'Dirección de ejemplo',
            'category_id' => random_int(1, 4),
            'city_id' => random_int(1, 4),
            'img1' => '1.png',
            'img2' => '2.png',
            'img3' => '3.png',
            'img4' => '4.png',
            'estacionamientos' => random_int(0, 9),
            'camas' => random_int(1, 9),
            'baños' => random_int(1, 6),
            'estado' => random_int(1, 2),
            'habitaciones' => random_int(0, 9),
            'living' => random_int(1, 2),
            'moneda' => random_int(1, 2),
            'cocina' => random_int(1, 2),
            'm2' => random_int(200, 400),
            'estado' => random_int(1, 2),
            'principal' => 1,
        ]);

        DB::table('categories')->insert([
            'created_at' => now(),
            'nombre' => 'Oficina'
        ]);

        DB::table('categories')->insert([
            'created_at' => now(),
            'nombre' => 'Departamento'
        ]);

        DB::table('categories')->insert([
            'created_at' => now(),
            'nombre' => 'Residencial'
        ]);

        DB::table('categories')->insert([
            'created_at' => now(),
            'nombre' => 'Casa'
        ]);

        DB::table('categories')->insert([
            'created_at' => now(),
            'nombre' => 'Comercio'
        ]);

    }
}
