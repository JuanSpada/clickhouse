<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('nombre');
            $table->float('precio');
            $table->string('direccion');
            $table->longText('descripcion');
            $table->integer('category_id');
            $table->integer('city_id');
            $table->string('img1');
            $table->string('img2')->nullable();
            $table->string('img3')->nullable();
            $table->string('img4')->nullable();
            $table->string('img5')->nullable();
            $table->integer('estacionamientos');
            $table->integer('camas');
            $table->integer('baños');
            $table->integer('habitaciones');
            $table->integer('living');
            $table->integer('moneda');
            $table->integer('cocina');
            $table->string('m2');
            $table->integer('estado');
            $table->integer('principal')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('properties');
    }
}
